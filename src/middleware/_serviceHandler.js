import Axios from "./_axios";
import { mapEndpointsContas } from "./_endpoints";
const env = require('./_env.config');

export const callGetMethod = async (api, endpoint, params) => {
  const parametros = params || "";
  const _endpoint = mapEndpointsContas.get(endpoint);
  return await Axios.get(`${api}/${parametros}${_endpoint}`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.error("erro no callGetMethod", error);
      return error.response;
    });
};

export const callPostMethod = (endpoint, body) => {
  const _endpoint = mapEndpointsContas.get(endpoint);
  return Axios.post(`${env.apiServerUrl}${_endpoint}/`, body)
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log("erro no callPostMethod", error);
      return error.response;
    });
};

export const callDeleteMethod = (endpoint, body) => {
  const _endpoint = mapEndpointsContas.get(endpoint);
  return Axios.delete(`${env.apiServerUrl}${_endpoint}`, {
    data: body
  })
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log("erro no callDeleteMethod", error);
      return error.response;
    });
};

export const callPatchMethod = (endpoint, body) => {
  const _endpoint = mapEndpointsContas.get(endpoint);
  return Axios.patch(`${env.apiServerUrl}${_endpoint}/`, body)
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log("erro no callPatchMethod", error);
      return error.response;
    });
};
