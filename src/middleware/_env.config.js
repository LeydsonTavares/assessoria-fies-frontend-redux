const config = {
    all: {
        env: process.env.NODE_ENV || 'development',
        apiServerUrl: '/api/',
    },
    development: {
        apiServerUrl: 'http://localhost:3300',
        apiIbgelUrl: "https://servicodados.ibge.gov.br/api/v1/localidades/estados",
        apiViaCeplUrl: "https://viacep.com.br/ws"
    },
    production: {
        apiServerUrl: 'https://assessoria-fies-backend.herokuapp.com',
        apiIbgelUrl: "https://servicodados.ibge.gov.br/api/v1/localidades/estados",
        apiViaCeplUrl: `https://viacep.com.br/ws`
    }
};

module.exports = Object.assign({}, config.all, config[config.all.env]);