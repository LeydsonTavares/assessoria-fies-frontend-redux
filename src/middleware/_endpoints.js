import * as CONSTANTS from "./_constants";

export const mapEndpointsContas = new Map();
mapEndpointsContas.set(CONSTANTS.AEF_CLIENTES, 'clientes');
mapEndpointsContas.set(CONSTANTS.AEF_REDE_PUBLICA, 'rede_publica');
mapEndpointsContas.set(CONSTANTS.AEF_GRAU_PARENTESCO, 'grau_parentesco');
mapEndpointsContas.set(CONSTANTS.AEF_ESTADO_CIVIL, 'estado_civil');
mapEndpointsContas.set(CONSTANTS.AEF_RACA_COR, 'raca_cor');
mapEndpointsContas.set(CONSTANTS.AEF_STATUS_PAGAMENTO, 'status_pagamento');
mapEndpointsContas.set(CONSTANTS.AEF_LOGRADOURO, '/json/');
mapEndpointsContas.set(CONSTANTS.AEF_MUNICIPIOS, '/municipios');

