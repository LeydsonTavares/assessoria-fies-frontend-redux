import React, { Fragment } from "react";
import {
  ListClientes,
  Chart,
  Deposits,
  CustomizedSteppers,
} from "../../../components/organisms";
import { useSelector } from "react-redux";
import { selectors } from "../../../redux/selectors/clientesSelectors";

const AppMain = (props) => {
  const currentComponent = useSelector(selectors.getComponent);
  const redirectComponent = () => {
    console.log("currentComponent", currentComponent);
    switch (currentComponent) {
      case "DASHBOARD":
        return <ListClientes isMobile={props.isMobile} />;

      case "REPORTS":
        return <Chart />;

      case "ADD_USER":
        return <Deposits />;

      case "ADD_CLIENT":
        return <CustomizedSteppers />;
      default:
        return <ListClientes isMobile={props.isMobile} />;
    }
  };
  return <Fragment>{redirectComponent()}</Fragment>;
};

export default AppMain;
