
import { AEF_CLIENTES, AEF_REDE_PUBLICA, AEF_LOGRADOURO, AEF_MUNICIPIOS, AEF_ESTADO_CIVIL, AEF_RACA_COR, AEF_STATUS_PAGAMENTO, AEF_GRAU_PARENTESCO } from '../../../middleware/_constants';
import { callGetMethod } from '../../../middleware/_serviceHandler';
const env = require('../../../middleware/_env.config');


export const getClientes = () => {
    return callGetMethod(env.apiServerUrl, AEF_CLIENTES)
        .then(response => response);
}

export const getEndereco = (cep) => {
    return callGetMethod(env.apiViaCeplUrl, AEF_LOGRADOURO, cep)
        .then(response => response);
}

export const getMunicipio = (idUF) => {
    return callGetMethod(env.apiViaCeplUrl, AEF_MUNICIPIOS, idUF)
        .then(response => response);
}
export const getEstadoCivil = () => {
    return callGetMethod(env.apiServerUrl, AEF_ESTADO_CIVIL)
        .then(response => response);
}
export const getRacaCor = () => {
    return callGetMethod(env.apiServerUrl, AEF_RACA_COR)
        .then(response => response);
}
export const getStatusPagamento = () => {
    return callGetMethod(env.apiServerUrl, AEF_STATUS_PAGAMENTO)
        .then(response => response);
}
export const getGrauParentesco = () => {
    return callGetMethod(env.apiServerUrl, AEF_GRAU_PARENTESCO)
        .then(response => response);
}
export const getRedePublica = () => {
    return callGetMethod(env.apiServerUrl, AEF_REDE_PUBLICA)
        .then(response => response);
}


