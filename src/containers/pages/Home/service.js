
import { AEF_CLIENTES } from '../../../middleware/_constants';
import { callGetMethod } from '../../../middleware/_serviceHandler';


export const getClientes = () => {
    return callGetMethod(AEF_CLIENTES)
        .then(response => response);
}

