import React, {PureComponent as Component} from 'react';
import PropTypes from 'prop-types';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.tryAgain = this.tryAgain.bind(this);
  }

  tryAgain() {
    this.props.history.push('/');
  }

  render() {
    console.log("@aplic", this.props.location.state); // eslint-disable-line
    return (
      <div>
        <div>
          <h1 type="h1" fontcolor="#fff">
            Página não encontrada
          </h1>
        </div>
        <div margin="24px 0 0 0 ">
          <h1 fontcolor="#fff">
            A página que você está tentando acessar não está disponível.
            <br />
            Verifique o endereço digitado e tente novamente.
          </h1>
        </div>
        <div>
          <div>
            <div ghost onClick={this.tryAgain}>
              Tentar novamente
            </div>
          </div>
        </div>
      </div>
    );
  }
}
Index.propTypes = {
  history: PropTypes.object.isRequired
};

export default Index;
