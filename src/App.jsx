import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { withRouter } from "react-router-dom";
import { Default } from "./config/themes";
import { Dashboard } from "./containers/pages";

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={Default}>
        <Dashboard />
      </ThemeProvider>
    );
  }
}

export default withRouter(App);
