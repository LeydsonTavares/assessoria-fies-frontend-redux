const Default = {
  typography: {
    fontFamily: [
      "Verdana", "Helvetica", "Arial", "sans-serif"
    ].join(','),
  },
  body: {
    bg: '#FFFFFF',
    color: '#900AE9',
    margin: '0',
    padding: '0',
    font: {
      family: 'Verdana',
      weight: 'Regular'
    }
  },
  breakpoints: {
    tablet: 768,
    desktop: 1024
  },
  titles: {
    font: 'Verdana',
    color: '#000',
    size: {
      h1: '4.2rem',
      h2: '2.4rem',
      h3: '1.8rem',
      h4: '1.6rem',
      h5: '1.3rem',
      h6: '1.0rem'
    }
  },
  buttons: {
    font: {
      family: 'Verdana',
      weight: 'Medium'
    }
  },
  inner: {
    opacity: 1
  },
  colors: {
    primary: '#d82482',
    secondary: '#900AE9',
    success: '#07CC14',
    error: '#f8562c',
    warning: '#ffc107',
    danger: '#f8562c',
    info: '#009DF7',
    light: '#f7f7f7',
    dark: '#222222',
    link: '#FFFFFF',
    gray: '#909090',
    white: '#fff',
    lightgray: '#cfcfcf'
  },
  gradients: {
    primary:
    {
      from: '#d0b71d',
      to: '#d82482'
      // to: '#89ba3f'
    },
    secondary: {
      from: '#ffff00',
      to: '#00ceff'
      // #00d318 42%,
    },
    success: {
      from: '#222222',
      to: '#FFFFFF'
    },
    error: {
      from: '#222222',
      to: '#FFFFFF'
    },
    warning: {
      from: '#222222',
      to: '#FFFFFF'
    },
    info: {
      from: '#222222',
      to: '#FFFFFF'
    },
    light: {
      from: '#222222',
      to: '#FFFFFF'
    },
    dark: {
      from: '#222222',
      to: '#FFFFFF'
    },
    link: {
      from: '#222222',
      to: '#FFFFFF'
    }
  }
};
export default Default;
export { Default };
