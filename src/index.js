import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { CookiesProvider } from 'react-cookie';
import { Provider } from 'react-redux';
import App from './App';
import "bootstrap/dist/css/bootstrap.min.css";
import { store } from '../src/redux/store';

ReactDOM.render(
    <Provider store={store}>
        <Router basename="/">
            <CookiesProvider>
                <App />
            </CookiesProvider>
        </Router>
    </Provider>,
    document.getElementById('root')
);

