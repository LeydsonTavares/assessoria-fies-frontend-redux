import { actionsTypes } from "../actions/_type"


const INITIAL_STATE = {
    objRetorno: {},
    currentComponent: "DASHBOARD"
}

const reducers = (state = INITIAL_STATE, { payload, type }) => {
    // const { objRetorno } = state;
    switch (type) {
        case actionsTypes.GET_CLIENTES:
            return { ...state, objRetorno: payload }
        case actionsTypes.SET_COMPONENT:
            return { ...state, currentComponent: payload }
        default:
            return state
    }
}

export { reducers }