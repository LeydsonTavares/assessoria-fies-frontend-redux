
import { combineReducers } from 'redux'

import { reducers as clientesReducers } from './clientesReducers'

const reducers = combineReducers({
  clientesReducers
})

export { reducers }