
import { createStore, applyMiddleware } from 'redux';
import { reducers } from '../reducers/clientesReducers';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';

const logger = createLogger({
  level: 'info',
  collapsed: false,
  logger: console,
  predicate: () => true // eslint-disable-line
});

let middlewares = [
  thunkMiddleware
];

if (process.env.NODE_ENV !== 'production') {
  middlewares = [...middlewares, logger];
}

const composeEnhancers = composeWithDevTools({
  serialize: true
});

const STORAGE_KEY = 'assessoria-fies-frontend-redux';

const saveToLocalStorage = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(STORAGE_KEY, serializedState);
  } catch (e) {
    console.log(e);
  }
};
const loadFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem(STORAGE_KEY);
    if (serializedState === null) return undefined;
    return JSON.parse(serializedState);
  } catch (e) {
    console.log(e);
    return undefined;
  }
};

const persistedState = loadFromLocalStorage();
const store = createStore(reducers, persistedState, composeEnhancers(applyMiddleware(...middlewares)));

store.subscribe(() => saveToLocalStorage(store.getState()));

export { store };