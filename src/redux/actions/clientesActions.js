import { actionsTypes } from "./_type";
import { callGetMethod } from "../../middleware/_serviceHandler";
import { AEF_CLIENTES } from "../../middleware/_constants";
const env = require('../../middleware/_env.config');

const actions = {
    getClientes: () => async (dispatch) => {
        return callGetMethod(env.apiServerUrl, AEF_CLIENTES)
            .then((response) => {
                return dispatch({
                    type: actionsTypes.GET_CLIENTES,
                    payload: response.data
                })
            });
    },
    changeCurrentComponent: (currentComponent) => (dispatch) => {
        return dispatch({
            type: actionsTypes.SET_COMPONENT,
            payload: currentComponent
        })
    }

};

export { actions };
