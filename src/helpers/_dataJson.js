export const estados = [
    {
        id: 11,
        sigla: "RO",
        nome: "Rondônia",
    },
    {
        id: 12,
        sigla: "AC",
        nome: "Acre",
    },
    {
        id: 13,
        sigla: "AM",
        nome: "Amazonas",
    },
    {
        id: 14,
        sigla: "RR",
        nome: "Roraima",
    },
    {
        id: 15,
        sigla: "PA",
        nome: "Pará",
    },
    {
        id: 16,
        sigla: "AP",
        nome: "Amapá",
    },
    {
        id: 17,
        sigla: "TO",
        nome: "Tocantins",
    },
    {
        id: 21,
        sigla: "MA",
        nome: "Maranhão",
    },
    {
        id: 22,
        sigla: "PI",
        nome: "Piauí",
    },
    {
        id: 23,
        sigla: "CE",
        nome: "Ceará",
    },
    {
        id: 24,
        sigla: "RN",
        nome: "Rio Grande do Norte",
    },
    {
        id: 25,
        sigla: "PB",
        nome: "Paraíba",
    },
    {
        id: 26,
        sigla: "PE",
        nome: "Pernambuco",
    },
    {
        id: 27,
        sigla: "AL",
        nome: "Alagoas",
    },
    {
        id: 28,
        sigla: "SE",
        nome: "Sergipe",
    },
    {
        id: 29,
        sigla: "BA",
        nome: "Bahia",
    },
    {
        id: 31,
        sigla: "MG",
        nome: "Minas Gerais",
    },
    {
        id: 32,
        sigla: "ES",
        nome: "Espírito Santo",
    },
    {
        id: 33,
        sigla: "RJ",
        nome: "Rio de Janeiro",
    },
    {
        id: 35,
        sigla: "SP",
        nome: "São Paulo",
    },
    {
        id: 41,
        sigla: "PR",
        nome: "Paraná",
    },
    {
        id: 42,
        sigla: "SC",
        nome: "Santa Catarina",
    },
    {
        id: 43,
        sigla: "RS",
        nome: "Rio Grande do Sul",
    },
    {
        id: 50,
        sigla: "MS",
        nome: "Mato Grosso do Sul",
    },
    {
        id: 51,
        sigla: "MT",
        nome: "Mato Grosso",
    },
    {
        id: 52,
        sigla: "GO",
        nome: "Goiás",
    },
    {
        id: 53,
        sigla: "DF",
        nome: "Distrito Federal",

    }
]


export const tiposEstadoCivil = [
    { id: "1", value: "Solteiro" },
    { id: "2", value: "Casado" },
    { id: "3", value: "Viúvo" },
    { id: "4", value: "Separado judicialmente" },
    { id: "5", value: "Divorciado" },
];

export const categorizacaoHumanaBrsaileira = [
    { id: "1", value: "Branca" },
    { id: "2", value: "Parda" },
    { id: "3", value: "Preta" },
    { id: "4", value: "Amarela" },
    { id: "5", value: "Indígena" },
];

export const grauParentesco = [
    { id: "1", value: "Mãe" },
    { id: "2", value: "Madrasta" },
    { id: "3", value: "Pai" },
    { id: "4", value: "Padastro" },
    { id: "5", value: "Cônjuge" },
    { id: "6", value: "Companheiro(a)" },
    { id: "7", value: "Filho(a)" },
    { id: "8", value: "Enteado(a)" },
    { id: "9", value: "Irmão" },
    { id: "10", value: "Irmão" },
];

export const genero = [{ id: 1, value: "Masculino" }, { id: 1, value: "Feminino" }];



export const redePublica = [
    { id: "1", value: "NÃO cursei o ensino médio em escola da rede pública." },
    {
        id: "2",
        value: "Cursei o ensino médio PARCIALMENTE em escola da rede pública.",
    },
    {
        id: "3",
        value: "Cursei o ensino médio COMPLETO em escola da rede pública.",
    },
];


export const getEstado = [
    {
        id: 11,
        sigla: 'RO',
        nome: 'Rondônia',
    },
    {
        id: 12,
        sigla: 'AC',
        nome: 'Acre',

    },
    {
        id: 13,
        sigla: 'AM',
        nome: 'Amazonas',

    },
    {
        id: 14,
        sigla: 'RR',
        nome: 'Roraima',

    },
    {
        id: 15,
        sigla: 'PA',
        nome: 'Pará',

    },
    {
        id: 16,
        sigla: 'AP',
        nome: 'Amapá',

    },
    {
        id: 17,
        sigla: 'TO',
        nome: 'Tocantins',

    },
    {
        id: 21,
        sigla: 'MA',
        nome: 'Maranhão',

    },
    {
        id: 22,
        sigla: 'PI',
        nome: 'Piauí',

    },
    {
        id: 23,
        sigla: 'CE',
        nome: 'Ceará',

    },
    {
        id: 24,
        sigla: 'RN',
        nome: 'Rio Grande do Norte',

    },
    {
        id: 25,
        sigla: 'PB',
        nome: 'Paraíba',

    },
    {
        id: 26,
        sigla: 'PE',
        nome: 'Pernambuco',

    },
    {
        id: 27,
        sigla: 'AL',
        nome: 'Alagoas',

    },
    {
        id: 28,
        sigla: 'SE',
        nome: 'Sergipe',

    },
    {
        id: 29,
        sigla: 'BA',
        nome: 'Bahia',

    },
    {
        id: 31,
        sigla: 'MG',
        nome: 'Minas Gerais',

    },
    {
        id: 32,
        sigla: 'ES',
        nome: 'Espírito Santo',

    },
    {
        id: 33,
        sigla: 'RJ',
        nome: 'Rio de Janeiro',

    },
    {
        id: 35,
        sigla: 'SP',
        nome: 'São Paulo',

    },
    {
        id: 41,
        sigla: 'PR',
        nome: 'Paraná',

    },
    {
        id: 42,
        sigla: 'SC',
        nome: 'Santa Catarina',

    },
    {
        id: 43,
        sigla: 'RS',
        nome: 'Rio Grande do Sul',

    },
    {
        id: 50,
        sigla: 'MS',
        nome: 'Mato Grosso do Sul',

    },
    {
        id: 51,
        sigla: 'MT',
        nome: 'Mato Grosso',

    },
    {
        id: 52,
        sigla: 'GO',
        nome: 'Goiás',

    },
    {
        id: 53,
        sigla: 'DF',
        nome: 'Distrito Federal',

    }
];