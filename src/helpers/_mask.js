export const cpfMask = cpf => {
    if (cpf) {
        return cpf
            .replace(/\D/g, '')  // Remove tudo o que não é dígito
            .replace(/(\d{3})(\d)/, '$1.$2')  // Coloca um ponto entre o terceiro e o quarto dígitos
            .replace(/(\d{3})(\d)/, '$1.$2') // Coloca um ponto entre o terceiro e o quarto dígitos de novo (para o segundo bloco de números)
            .replace(/(\d{3})(\d{1,2})/, '$1-$2')  // Coloca um hífen entre o terceiro e o quarto dígitos
    }
}

export const cellphoneMask = number => {
    if (number) {
        number = number.replace(/\D/g, '');
        number = number.replace(/^(\d{2})(\d)/g, '($1) $2');
        number = number.replace(/(\d)(\d{4})$/, '$1-$2');
        return number;
    } else {
        return number;
    }
};

// /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((18|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((18|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/

export const cepMask = (cep) => {
    if (cep) {
        return cep.replace(/\D/g, "")
            .replace(/^(\d{5})(\d)/, "$1-$2")
    }

}

export const intMask = (str) => {
    if (str) {
        return str.replace(/\D/g, "")
    }

}

export const dateMask = (str) => {
    function pad(x) { return ((('' + x).length === 2) ? '' : '0') + x; }
    var m = str.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/)
        , d = (m) ? new Date(m[3], m[2] - 1, m[1]) : null
        , matchesPadded = (d && (str === [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')))
        , matchesNonPadded = (d && (str === [d.getDate(), d.getMonth() + 1, d.getFullYear()].join('/')));
    console.log((matchesPadded || matchesNonPadded) ? d : null);
    return (matchesPadded || matchesNonPadded) ? d : null;
}

export const realMask = (double) => {
    if (double) {
        return double.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
    }
}