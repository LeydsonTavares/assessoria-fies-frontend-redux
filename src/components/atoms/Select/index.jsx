import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { applyBreakpoints } from "../../../config/styles/mixins";

const SelectStyle = styled.div`
  .custom-select {
    position: ${props => props.position};
    font-family: Simplon;
    width: ${props => props.width};
    min-width: ${props => props.minWidth};
    max-width: ${props => props.maxWidth};
    height: ${props => props.height};
    min-height: ${props => props.minHeight};
    max-height: ${props => props.maxHeight};
    margin: ${props => props.margin};
    border-radius: ${props => props.radius};
    ${props => typeof props.border === "string" && `border: ${props.border}`};
    ${props =>
      typeof props.border === "object" &&
      `
      border-top: ${props.border.top || "none"};
      border-right: ${props.border.right || "none"};
      border-bottom: ${props.border.bottom};
      border-left: ${props.border.left || "none"};
    `};
  }

  ${applyBreakpoints}
`;

const Select = props => {
  return (
    <SelectStyle {...props}>
      <div onClick={props.onClick} key={props.chave}>
        <select
          key={props.chave}
          id={`select-motivos${props.chave}`}
          data-context={props.dataContext}
          className="custom-select"
        >
          <option value={-1} key={-1}>
            Selecione o motivo...
          </option>
          {props.items.map((e, i) => {
            return (
              <option
                key={`${props.chave}${i}`}
                className="select-items"
                value={e.idJustificativa}
              >
                {e.descricao}
              </option>
            );
          })}
        </select>
      </div>
    </SelectStyle>
  );
};

Select.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  position: PropTypes.string
};

Select.defaultProps = {
  width: "auto",
  height: "auto",
  position: "relative"
};

export { Select };
export default Select;
