import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import * as mixins from '../../../config';
import { Default as theme } from '../../../config/themes';


const StyledTag = styled.span`
  display: inline-block;
  
  flex-shrink: ${({ shrink }) => shrink};
  width: auto;
  height: ${({ height }) => height || '16px'};

  border-radius: 4px;
  background-color: ${({ color, backgroundColor }) => color ? (color.indexOf('#') >= 0 ? color : theme.colors[color]) : (backgroundColor.indexOf('#') >= 0 ? backgroundColor : theme.colors[backgroundColor])};
  padding: ${({ padding }) => mixins.setSizes(padding)};
  margin: ${({ margin }) => mixins.setSizes(margin)};
  
  font-family: 'SimplonHeadline', sans-serif;
  font-size: ${({ fontSize }) => mixins.rem(fontSize)};
  color: ${({ color, fontColor }) => color ? '#fff' : (fontColor.indexOf('#') >= 0 ? fontColor : theme.colors[fontColor])};
  text-transform: ${({ textTransform }) => textTransform || 'uppercase'};
  text-align: center;
  line-height: ${({ lineHeight, height }) => lineHeight || height || '16px'};
  
  box-sizing: content-box;
  white-space: nowrap;
`;

const Tag = props => <StyledTag {...props}>{props.children}</StyledTag>;

Tag.propTypes = {
  fontSize: PropTypes.number,
  fontColor: PropTypes.string,
  color: PropTypes.string,
  padding: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.arrayOf(PropTypes.number)
  ]),
  margin: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.arrayOf(PropTypes.number)
  ]),
  backgroundColor: PropTypes.string,
  shrink: PropTypes.number
};

Tag.defaultProps = {
  fontSize: 10,
  color: undefined,
  fontColor: 'success',
  padding: [2, 8],
  margin: [0],
  backgroundColor: '#f5f5f5',
  shrink: 1
};

export { Tag };
export default Tag;
