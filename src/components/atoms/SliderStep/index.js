import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { prop } from 'styled-tools';
import { applyBreakpoints } from '../../../config/styles/mixins';
import Container from '../Container';
import Text from '../Text';
import { css } from 'styled-components';
import Browser from '../../../helpers/browserChecker'
import Nsw from '../../../helpers/deviceChecker'


const SliderStepType = props => <input type="range" {...props} />;

const SliderStepStyle = styled(SliderStepType)`      

// Default

    /*removes default webkit styles*/
    -webkit-appearance: none;    
    /*fix for FF unable to apply focus style bug */
    border: 1px solid white;  
    /*required for proper track sizing in FF*/
    width: 100%;
    height: 100%;
    outline: none;
    max-width: 884px;
    min-height: 100px;

//Chrome
::-webkit-slider-runnable-track {
    width: 100%;
    height: ${props => props.track_height}px;
    background: ${props => props.bgcolor};
    transition: background 10ms ease-in;
}

::-webkit-slider-thumb {
  -webkit-appearance: none;
  border: solid 1px;
  height:${props => props.thumb_height};
  width: ${prop('thumb_height')};
  margin-top: -8.5px;
  border-radius: 50%;
  background: ${props => props.thumb_color}; 
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  border: solid 2px #dbdbdb;

}

//Mozilla Firefox

::-moz-range-track {
  width: 100%;
  height: ${props => props.track_height}px;
  background: ${props => props.bgcolor};
  border: none;
}

::-moz-range-thumb {
  border: none;
  height:${props => props.thumb_height};
  width: ${prop('thumb_height')};
  border-radius: 50%;
  background: ${props => props.thumb_color}; 
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  border: solid 2px #dbdbdb;
}

::-moz-range-progress {
  background-color:${props => props.bgcolor};
  height: ${props => props.track_height}px;
  border-top-left-radius: 15px;
  border-bottom-left-radius: 15px;
}

/*hide the outline behind the border*/
::-moz-focusring{
  outline: 1px solid white;
  outline-offset: -1px;
}

//Internet Explorer

::-ms-track {
  width: 100%;
  height: ${props => props.track_height}px;
  
  /*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */
  background: transparent;
  
  /*leave room for the larger thumb to overflow with a transparent border */
  border-color: transparent;
  border-width: 6px 0;

  /*remove default tick marks*/
  color: transparent;
}

::-ms-fill-lower {
  background: ${props => props.bgcolor};

}

::-ms-fill-upper {
  background: ${props => props.bgcolor};
}

::-ms-thumb {
  height: ${props => props.thumb_height};
  width: ${prop('thumb_height')};
  border-radius: 50%;
  background: ${props => props.thumb_color}; 
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  border: solid 2px #dbdbdb;
}

::-ms-tooltip {
  display: none;
}

${applyBreakpoints}
`;


const Step = styled.div`
    display: flex;
    justify-content: space-between;
    width: 100%;
    margin-top: -35px;
    max-width: 884px;
    height: 160px;
    z-index: 1;
  

    p {
      font-size: 11px;
      font-weight: 500;
      font-style: normal;
      font-stretch: normal;
      line-height: normal;
      letter-spacing: normal;
      text-align: center;
      color: #aeaeae;
      margin: 50px -15px 0px;
    }

    div#ticks {
      width: 8px;
      height: 24px; 
      margin: 0px 5px;
    }
    ${applyBreakpoints}

`
const CardValue = (props) => {
  return (
    <Container justify={'flex-start'} alignitems={'baseline'} alignself={'baseline'} breakpoints=
    {{tablet: css`max-width: 280px`}}
      padding={[24, 40, 32, 40]} radius={4} direction={'row'} border={'1px solid #dbdbdb'} height={'82px'} minheight={'82px'} width={'100%'}>
      <Text width={'auto'} margin={[0, 5, 0, 0]} fontWeight='Medium' fontSize={24} fontcolor="#00d213"> {'R$'}</Text>
      <Text width={100} fontWeight='Medium' fontSize={32} fontcolor="#00d213"> {props.value},00 </Text>
    </Container >
  )
}


const SliderStep = (props) => {
  return (
    <Container justify={'flex-start'} alignitems={'flex-start'}>
      <CardValue value={props.steps[props.value]} />
      <SliderStepStyle  {...props} breakpoints={{
          tablet: css`
          ::-webkit-slider-thumb { 
            margin-top: -18.5px; 
            height:42px; 
            width:42px; 
          }
          ::-moz-range-thumb {
            height:42px;
            width:42px;
          }
          ::-ms-thumb {
            height:42px;
            width:42px;
          }
          `
        }}></SliderStepStyle >
      <Step breakpoints={{
          tablet: css`  
          div#ticks {
            width: 12px;
            height: 32px;
            margin: -30px 15px 0px;
          }
          p {
            font-size: 18px;
          }
          `}}>

        {props.steps.map((item, index) => {
          return <div id="ticks" onClick={props.handleClick} value={index}
            style={{
              marginTop: Browser.isIE() ? '-40px' : '-30px',
              backgroundColor: item !== props.steps[props.value] ? '#f5f5f5' : 'transparent',
              boxShadow: item !== props.steps[props.value] ? '0 2px 6px 0 rgba(0, 0, 0, 0.14)' : 'none'
            }} >
            <p key={index} style={{
              marginLeft: (Nsw.isMobile() && index == 0) ? '-5px' : '-15px',
              marginRight: (Nsw.isMobile() && index == props.steps.length -1 ) ? '-5px' : '-15px'
              }} >R${item}</p>
          </div>
        })}
      </Step>
    </Container >

  );
};

SliderStep.propTypes = {
  thumb_height: PropTypes.string,
  thumb_color: PropTypes.string,
  bgcolor: PropTypes.string,
  track_height: PropTypes.number,
  value: PropTypes.string.isRequired,
  steps: PropTypes.oneOfType([PropTypes.number, PropTypes.array]).isRequired,
};

SliderStep.defaultProps = {
  height: '100%',
  thumb_height: '24px',
  thumb_color: '#00d213',
  bgcolor: '#00d213',
  track_height: 7
};

export { SliderStep };
export default SliderStep;
