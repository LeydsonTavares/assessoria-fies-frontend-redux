import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import * as mixins from '../../../config';
import { applyBreakpoints } from '../../../config/styles/mixins';
import { ifProp, prop } from 'styled-tools';

const ToggleStyle = styled.div`
  font-family: ${prop('fontFamily')};
  padding: ${props =>
    props.paddingToggle && mixins.setSizes(props.paddingToggle)};
  display: ${props => props.display};

  input {
    position: absolute !important;
    clip: rect(0, 0, 0, 0);
  }

  label {
    height: ${props => props.height};
    width: ${props => props.width};
    font-size: ${props => props.fontSize && mixins.rem(props.fontSize)};
    background-color: ${props => props.bgColor};
    border: ${props => props.border};
    box-shadow: ${props => props.boxShadow};
    transition: ${props => props.transition};
    font-weight: ${props => props.fontWeight};
    text-align: ${ifProp('align', prop('align'))};
    padding: ${props =>
      props.textPadding && mixins.setSizes(props.textPadding)};
    color: ${props => props.color};
  }

  label:hover {
    cursor: pointer;
  }

  label:first-of-type {
    border-radius: ${props => props.radiusLeft};
  }

  label:last-of-type {
    border-radius: ${props => props.radiusRight};
  }

  ${applyBreakpoints};
`;

const Toggle = props => {
  return (
    <ToggleStyle {...props}>
      <input
        data-context={props.dataContextLeft}
        checked={!props.checked}
        type='radio'
        id={`switch_left_${props.chave}`}
        name='switchToggle'
      />
      <label
        htmlFor={`switch_left_${props.chave}`}
        style={{
          backgroundColor: !props.checked ? props.bgColorSL : '',
          color: !props.checked ? props.textColor : ''
        }}>
        {!props.checked ? props.leftLabel : ' '}
      </label>

      <input
        checked={props.checked}
        type='radio'
        id={`switch_right_${props.chave}`}
        name='switchToggle'
      />
      <label
        htmlFor={`switch_right_${props.chave}`}
        data-context={props.dataContextRight}
        style={{
          backgroundColor: props.checked ? props.bgColorSR : '',
          color: props.checked ? props.textColor : ''
        }}>
        {props.checked ? props.rightLabel : ' '}
      </label>
    </ToggleStyle>
  );
};

Toggle.propTypes = {
  fontFamily: PropTypes.string,
  paddingToggle: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  height: PropTypes.string,
  width: PropTypes.string,
  fontSize: PropTypes.number,
  bgColor: PropTypes.string,
  border: PropTypes.string,
  boxShadow: PropTypes.string,
  transition: PropTypes.string,
  fontWeight: PropTypes.oneOf(['Light', 'Regular', 'Medium', 'Bold']),
  align: PropTypes.oneOf(['left', 'center', 'right', 'justify']),
  textPadding: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  display: PropTypes.string,
  color: PropTypes.string,
  bgColorSL: PropTypes.string,
  bgColorSR: PropTypes.string,
  textColor: PropTypes.string,
  radius: PropTypes.string,
  rightLabel: PropTypes.string,
  leftLabel: PropTypes.string,
  checked: PropTypes.bool.isRequired
};

Toggle.defaultProps = {
  fontFamily: 'Simplon',
  paddingToggle: 0,
  height: '23px',
  width: '30px',
  fontSize: 10,
  bgColor: '#e4e4e4',
  border: '1px solid rgba(0, 0, 0, 0.2)',
  boxShadow:
    'inset 0 1px 3px rgba(0, 0, 0, 0.3), 0 1px rgba(255, 255, 255, 0.1);',
  transition: 'all 0.2s ease-in-out',
  fontWeight: 'Regular',
  align: 'center',
  textPadding: [3, 12],
  display: 'flex',
  color: 'rgba(0, 0, 0, 0.6)',
  bgColorSL: 'gray',
  bgColorSR: '#226f22',
  textColor: '#fff',
  radiusLeft: '4px 0 0 4px',
  radiusRight: '0 4px 4px 0',
  rightLabel: '',
  leftLabel: '',
  checked: false
};

export { Toggle };
export default Toggle;
