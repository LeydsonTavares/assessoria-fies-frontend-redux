import React from 'react';
import styled, { css } from 'styled-components';
import { ifProp, prop, switchProp } from 'styled-tools';
import PropTypes from 'prop-types';
import * as mixins from '../../../config';
import { applyBreakpoints } from '../../../config/styles/mixins';


const ButtonType = (props) => {
  switch (props.type) {
    case 'a':
      return <a type={props.type} onClick={props.onClear} href={props.href} className={props.className} {...props}>{props.children}</a>;
    case 'button':
    default:
      return <button type={props.type} onClick={props.onClick} className={props.className} {...props}>{props.children}</button>;
  }
};

/* eslint-disable */
const ButtonStyle = styled(ButtonType)`
  display: ${props => props.display};
  ${props => props.flex && `
    display: flex;
    flex-direction: ${props.direction || 'column'};
    justify-content: ${props.justify || 'space-between'};
    align-items: flex-start;
    align-content: center;
  `};
  ${props => props.alignself && `align-self: ${props.alignself}`};
  width: ${props => (props.full ? '100%' : props.width)};
  height: ${props => (props.height && props.height)};
  min-width: ${props => (props.minwidth ? props.minwidth : props.width)};
  min-height: ${props => (props.minheight ? props.minheight : props.height)};
  max-width: ${props => (props.maxWidth ? props.maxWidth : props.width)};
  max-height: ${props => (props.maxHeight ? props.maxHeight : props.height)};
  cursor: pointer;
  opacity: 1;
  margin: ${props => props.margin && `${mixins.setSizes(props.margin)}`};
  transition: all 0.3s ease;
  font-family: ${prop('fontFamily')};
  font-weight: ${props => props.fontWeight && `${mixins.fontWeight(props.fontWeight)}`};
  font-size: ${props => props.fontSize && `${mixins.rem(props.fontSize)}`};
  color: ${props => props.color};
  text-transform: none;
  ${ifProp('uppercase', 'text-transform: uppercase')};
  padding: ${props => props.padding && `${mixins.setSizes(props.padding)}`};
  position: relative;
  background-image: ${props => props.backgroundImage};
  background: ${props => props.bgcolor};
  box-shadow: ${props => props.shadow && `${mixins.setSizes(props.shadow)} ${props.shadow_color ? `rgba(${props.shadow_color})` : 'rgba(204, 204, 204, 0.3)'}`};
  ${switchProp('themecolor', {
  primary: css`
      border-color: ${prop('theme.colors.primary')};
      background: ${prop('theme.colors.primary')};
      color: ${props => props.outline ? prop('theme.colors.primary') : props.color}`,
  secondary: css`
      border-color: ${prop('theme.colors.secondary')};
      background: ${prop('theme.colors.secondary')};`,
  success: css`
      border-color: ${prop('theme.colors.success')};
      background: ${prop('theme.colors.success')};`,
  error: css`
      border-color: ${prop('theme.colors.error')};
      background: ${prop('theme.colors.error')};`,
  warning: css`
      border-color: ${prop('theme.colors.warning')};
      background: ${prop('theme.colors.warning')};`,
  info: css`
      border-color: ${prop('theme.colors.info')};
      background: ${prop('theme.colors.info')};`,
  light: css`
      border-color: ${prop('theme.colors.light')};
      background: ${prop('theme.colors.light')};`,
  dark: css`
      border-color: ${prop('theme.colors.dark')};
      background: ${prop('theme.colors.dark')};`,
  link: css`
      border-color: ${prop('theme.colors.link')};
      background: ${prop('theme.colors.link')};`,
  white: css`
      border-color: ${prop('theme.colors.white')};
      background: ${prop('theme.colors.white')};`,
  black: css`
      border-color: ${prop('theme.colors.black')};
      background: ${prop('theme.colors.black')};`,
  none: css`
      background: ${props => props.bgcolor};`
})};
  ${ifProp('outline', `
    background: transparent;
  `)};
  ${switchProp(prop('type', 'button'), {
  a: css`
      text-align: center;
      text-decoration: ${props => props.nodecoration ? 'none' : 'underline'};
      background: transparent;
      color: ${props => props.color};
      border: none;
    `,
  button: css`
      ${ifProp('outline', '', 'box-shadow: 3px 3px 8px 0px rgba(174, 174, 174, 1);')};
        min-width: 137px;
        min-height: 44px;
        border: ${props => (props.border ? props.border : 'none')};
        border-radius: 5px;
        font-size: ${props => mixins.rem((props.fontSize * 0.8))}
      
    `,
  icon: css`
      background: transparent;
      border: none;
      min-width: unset;
      min-height: unset;
    `
})};
  ${ifProp('disabled', `
    opacity: 0.5;
    cursor: not-allowed;
  `)};
  &:focus{
    outline: none
  }
  ${applyBreakpoints}

`;

/* eslint-enable */
const Button = (props) => {
  const handleClick = () => {
    const { customClick, customClickParams, onClick } = props;
    if (customClick) {
      return customClick(customClickParams);
    }
    return onClick;
  };
  return <ButtonStyle onClick={handleClick} {...props}>{props.children}</ButtonStyle>;
};

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.string
  ]),
  display: PropTypes.string,
  themecolor: PropTypes.oneOf(['primary', 'secondary', 'success', 'error', 'warning', 'info', 'light', 'dark', 'link', 'white', 'black', 'none']),
  type: PropTypes.oneOf(['a', 'button', 'icon']),
  width: PropTypes.string,
  height: PropTypes.string,
  minwidth: PropTypes.string,
  maxWidth: PropTypes.string,
  maxHeight: PropTypes.string,
  minheight: PropTypes.string,
  bgcolor: PropTypes.string,
  border: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.shape({
      top: PropTypes.string,
      bottom: PropTypes.string,
      left: PropTypes.string,
      right: PropTypes.string
    })
  ]),
  backgroundImage: PropTypes.string,
  margin: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  padding: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  radius: PropTypes.arrayOf(PropTypes.number),
  fontFamily: PropTypes.string,
  fontWeight: PropTypes.oneOf(['Light', 'Regular', 'Medium', 'Bold']),
  fontSize: PropTypes.number,
  color: PropTypes.string,
  shadow: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.number), PropTypes.number]),
  shadow_color: PropTypes.string,
  uppercase: PropTypes.bool,
  nodecoration: PropTypes.bool,
  flex: PropTypes.bool,
  customclick: PropTypes.func,
  customclickparams: PropTypes.object
};

Button.defaultProps = {
  backgroundImage: 'linear-gradient(281deg, #f8562c, #e92555);',
  display: 'block',
  type: 'button',
  themecolor: 'none',
  children: 'Botão',
  width: 'auto',
  height: 'auto',
  minwidth: '157px',
  minheight: '54px',
  margin: [10, 5],
  padding: [5],
  radius: [4],
  fontFamily: 'Simplon',
  fontSize: 16,
  shadow: 3,
  shadow_color: '#dbdbdb',
  nodecoration: false,
  customClickParams: { param: true },
  customClick: p => p
};

export { Button };
export default Button;
