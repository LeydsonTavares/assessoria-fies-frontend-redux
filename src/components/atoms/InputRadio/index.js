import React from 'react';
import styled, { css } from 'styled-components';
import { applyBreakpoints } from '../../../config/styles/mixins';

const InputRadioStyle = styled.label`
    
    .container {
        display: ${props => props.display};
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    
    /* Hide the browser's default radio button */
    .container input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }
    
    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: ${props => props.height};
        width: ${props => props.width};
        border: 2px solid #d82482;
        border-radius: 50%;
    }
        
    /* When the radio button is checked, add a purple background */
    .container input:checked ~ .checkmark {
        background-color: #ffffff;
    }
    
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }
    
    /* Show the indicator (dot/circle) when checked */
    .container input:checked ~ .checkmark:after {
        display: block;
    }
    
    /* Style the indicator (dot/circle) */
    .container .checkmark:after {
        top: ${props => props.circleTop};
        left: ${props => props.circleLeft};
        width: ${props => props.circleHeight};
        height: ${props => props.circleWidth};
        border-radius: 50%;
        background: #d82482;
    }
    ${applyBreakpoints}
`

const InputRadio = (props) => {
    return (
        <InputRadioStyle {...props}>
            <label className='container' {...props}>
                <input type='radio' value={props.value} name={props.name}></input>
                <span className='checkmark'></span>
            </label>
        </InputRadioStyle>
    )
}

export { InputRadio }
export default InputRadio