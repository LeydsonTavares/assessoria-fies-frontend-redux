import React from "react";
import { Container } from '..'
import styled from "styled-components";
import PropTypes from 'prop-types';
import { applyBreakpoints } from "../../../config/styles/mixins";
import * as mixins from '../../../config';

const CardType = props => <Container {...props} />;

const CardStyle = styled(CardType)`
    padding: ${props => props.padding && `${mixins.setSizes(props.padding)}`};
    border: 1px solid #d2d2d2 !important;
    border-radius: 4px !important;
    width: ${props => props.width};
    height: ${props => props.height};
    margin: ${props => props.margin && `${mixins.setSizes(props.margin)}`};
    min-width: ${props => props.minwidth};
    max-width: ${props => props.maxwidth};
    ${applyBreakpoints}
`


const Card = (props) => {
    return (
        <CardStyle {...props}>
            {props.children}
        </CardStyle>
    )
};


Card.propTypes = {
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    maxwidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    minwidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    margin: PropTypes.oneOfType([PropTypes.number, PropTypes.array, PropTypes.string]),
    padding: PropTypes.oneOfType([PropTypes.number, PropTypes.array, PropTypes.string])
}

Card.defaultProps = {
    width: '100%',
    heigth: 'auto',
    margin: 0,
    padding: [30]
}

export { Card }
export default Card
