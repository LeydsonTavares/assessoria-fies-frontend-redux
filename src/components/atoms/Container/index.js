import React from 'react';
import styled, {css} from 'styled-components';
import {prop, switchProp} from 'styled-tools';
import PropTypes from 'prop-types';
import * as mixins from '../../../config';
import {applyBreakpoints} from '../../../config/styles/mixins';

const ContainerType = (props) => {
  switch (props.type) {
    case 'form':
      return (
        <form {...props}>
          {props.children}
        </form>
      );
    case 'ul':
      return (
        <ul {...props}>
          {props.children}
        </ul>
      );
    case 'ol':
      return (
        <ol {...props}>
          {props.children}
        </ol>
      );
    case 'fieldset':
      return (
        <fieldset {...props}>
          {props.children}
        </fieldset>
      );
    case 'div':
    default:
      return (
        <div {...props}>
          {props.children}
        </div>
      );
  }
};

const ContainerStyle = styled(ContainerType)`
  display: ${props => props.display};
  ${switchProp('display', {
    flex: css`
        flex-direction: ${props => props.direction};
        justify-content: ${props => props.justify};
        align-content: ${props => props.aligncontent};
        align-items: ${props => props.alignitems};
        flex-wrap: ${props => props.flexwrap};
        flex-grow: ${props => props.grow};
        flex-shrink: ${props => props.shrink};
        order: ${props => props.order};
        `
  })};
  align-self: ${props => props.alignself};
  
  position: ${({ position }) => position || "static"};
  ${({top}) => top && `top: ${typeof top === 'number' ? top+'px' : top}`};
  ${({left}) => left && `left: ${typeof left === 'number' ? left+'px' : left}`};
  ${({bottom}) => bottom && `bottom: ${typeof bottom === 'number' ? bottom+'px' : bottom}`};
  ${({right}) => right && `right: ${typeof right === 'number' ? right+'px' : right}`};
  
  cursor: ${props => props.cursor};
  width: ${props => props.width};
  min-width: ${props => props.minwidth};
  max-width: ${props => props.maxwidth};
  height: ${props => props.height};
  min-height: ${props => props.minheight};
  max-height: ${props => props.maxheight};
  margin: ${props => props.margin && `${mixins.setSizes(props.margin)}`};
  padding: ${props => props.padding && `${mixins.setSizes(props.padding)}`};
  background-color: ${props => props.bgcolor};
  background: ${props => props.bgmedia
    && `background: url(${props.bgmedia.desktop}) center top no-repeat ${prop('bgcolor')};
    background-size: cover;
    @media (max-width: 800px) {
      background: url(${props.bgmedia.tablet}) center top no-repeat ${prop('bgcolor')};
      background-size: cover;
    };
    @media (max-width: 480px) {
      background: url(${props.bgmedia.mobile}) center top no-repeat ${prop('bgcolor')};
      background-size: cover;
    };`};
  
  ${props => typeof props.border === 'string' && `border: ${props.border}`};
  ${props => typeof props.border === 'object' && `
      border-top: ${props.border.top || 'none'};
      border-right: ${props.border.right || 'none'};
      border-bottom: ${props.border.bottom || 'none'};
      border-left: ${props.border.left || 'none'};
      `};
  box-shadow: ${props => props.shadow && `${mixins.setSizes(props.shadow)} ${props.shadow_color ? `rgba(${props.shadow_color})` : 'rgba(204, 204, 204, 0.3)'}`};
  border-radius: ${props => props.radius && `${mixins.setSizes(props.radius)}`}
  
  ${switchProp('type', {
    ul: css`list-style-type: none`
  })};
  
  overflow-scrolling: touch;
  transition: all 0.3s ease;
  z-index: ${props => props.zindex};
  ${props => props.overflowx && `overflow-x: ${props.overflowx}`}
  ${props => props.overflowy && `overflow-y: ${props.overflowy}`}
  ${({whitespace}) => whitespace && `white-space: ${whitespace}`}

  ${applyBreakpoints}
`;

const Container = (props) => {
  return (
    <ContainerStyle {...props}>
      {props.children}
    </ContainerStyle>
  );
};

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  display: PropTypes.oneOf(['flex', 'block', 'inline-block']),
  type: PropTypes.oneOf(['div', 'ul', 'ol', 'form', 'fieldset']),
  direction: PropTypes.oneOf(['column', 'row']),
  justify: PropTypes.oneOf(['flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'space-evenly']),
  aligncontent: PropTypes.oneOf(['flex-start', 'flex-end', 'center', 'space-between', 'space-around', 'stretch']),
  alignitems: PropTypes.oneOf(['flex-start', 'flex-end', 'center', 'baseline', 'stretch']),
  alignself: PropTypes.oneOf(['flex-start', 'flex-end', 'center', 'baseline', 'stretch']),
  flexwrap: PropTypes.oneOf(['nowrap', 'wrap', 'wrap-reverse']),
  margin: PropTypes.oneOfType([PropTypes.number, PropTypes.array, PropTypes.string]),
  padding: PropTypes.oneOfType([PropTypes.number, PropTypes.array, PropTypes.string]),
  width: PropTypes.string,
  cursor: PropTypes.string,
  minwidth: PropTypes.string,
  maxwidth: PropTypes.string,
  height: PropTypes.string,
  minheight: PropTypes.string,
  maxheight: PropTypes.string,
  position: PropTypes.oneOf(['relative', 'absolute', 'static', 'fixed']),
  zindex: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  bgcolor: PropTypes.string,
  bgmedia: PropTypes.shape({
    desktop: PropTypes.string,
    tablet: PropTypes.string,
    mobile: PropTypes.string
  }),
  shadow: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.number), PropTypes.number]),
  shadow_color: PropTypes.arrayOf(PropTypes.number),
  border: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  radius: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  overflowx: PropTypes.oneOf(['hidden', 'visible', 'auto', 'scroll']),
  overflowy: PropTypes.oneOf(['hidden', 'visible', 'auto', 'scroll'])
};

Container.defaultProps = {
  type: 'div',
  display: 'flex',
  direction: 'column',
  justify: 'space-between',
  aligncontent: 'center',
  alignitems: 'center',
  flexwrap: 'nowrap',
  alignself: 'center',
  margin: 0,
  padding: 0,
  width: '100%',
  minwidth: '',
  maxwidth: '',
  height: 'auto',
  minheight: '',
  maxheight: '',
  position: 'relative',
  zindex: 'initial',
  bgcolor: 'transparent',
  bgmedia: {
    desktop: '',
    tablet: '',
    mobile: ''
  },
  border: 'none',
  radius: 0,
  shadow: 0,
  shadow_color: [0],
  overflowy: 'hidden',
  overflowx: 'hidden',
  order: 'initial',
  grow: 'initial',
  shrink: 'initial',
  cursor: 'default'
};

export {Container};
export default Container;
