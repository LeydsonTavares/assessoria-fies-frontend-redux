const breakpoint = {
  small: {
    size: 320,
    block: 40,
    spaceBtw: 10,
    spaceArd: 15
  },
  large: {
    size: 1366,
    block: 70,
    spaceBtw: 30,
    spaceArd: 98
  }
};

const media = (qtd) => {
  const allA = {};
  let mQuery = '';

  qtd.map((item) => {
    return Object.keys(breakpoint).filter((k, i) => {
      return k;
    }).forEach((k) => {
      allA[k] = `@media (min-width: ${breakpoint[k].size - 1}px){
               width: ${(item * breakpoint[k].block) + ((item - 1) * breakpoint[k].spaceBtw)}px;
               &:not(:first-child) {
                 margin-left: ${breakpoint[k].spaceBtw}px;
               }
             }`;
      mQuery += allA[k];
    });
  });
  return mQuery;
};


const container = () => {
  const allA = {};
  let mQuery = '';
  Object.keys(breakpoint).filter((k) => {
    return k;
  }).forEach((k) => {
    allA[k] = `
            flex-direction: column;
            @media (min-width: ${breakpoint[k].size - 1}px){
           width: ${(breakpoint[k].size) - (breakpoint[k].spaceArd * 2)}px;
           margin: 0 auto;
         }`;
    mQuery += allA[k];
  });
  return mQuery;
};

export {breakpoint, media, container};
