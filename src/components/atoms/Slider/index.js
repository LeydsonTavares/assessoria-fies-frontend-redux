import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { prop } from 'styled-tools';
import { applyBreakpoints } from '../../../config/styles/mixins';


const SliderType = props => <input type="range" {...props} />;

const SliderStyle = styled(SliderType)`      

// Default

    /*removes default webkit styles*/
    -webkit-appearance: none;    
    /*fix for FF unable to apply focus style bug */
    border: 1px solid white;  
    /*required for proper track sizing in FF*/
    width: 100%;
    height: 100%;
    outline: none;
    max-width: 480px;
    min-height: 100px;

//Chrome
::-webkit-slider-runnable-track {
    width: 100%;
    height: ${props => props.track_height}px;
    background: linear-gradient(to right, 
      ${props => props.upper_color} 0%, 
      ${props => props.upper_color} ${props => (props.value * 100) / props.max}%, 
      ${props => props.lower_color}  ${props => (props.value * 100) / props.max}%,    
      ${props => props.lower_color}  ${props => props.max}%);
    border-radius: 15px;
    transition: background 10ms ease-in;
}

::-webkit-slider-thumb {
  -webkit-appearance: none;
  border: solid 1px;
  height:${props => props.thumb_height};
  width: ${prop('thumb_height')};
  margin-top: -${props => (props.track_height) / 2}px;
  border-radius: 50%;
  background: ${props => props.thumb_color}; 
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  border: solid 2px #dbdbdb;

}

//Mozilla Firefox

::-moz-range-track {
  width: 100%;
  height: ${props => props.track_height}px;
  background: ${props => props.lower_color};
  border: none;
  border-radius: 15px;
}

::-moz-range-thumb {
  border: none;
  height:${props => props.thumb_height};
  width: ${prop('thumb_height')};
 border-radius: 50%;
  background: ${props => props.thumb_color}; 
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  border: solid 2px #dbdbdb;
}

::-moz-range-progress {
  background-color:${props => props.upper_color};
  height: ${props => props.track_height}px;
  border-top-left-radius: 15px;
  border-bottom-left-radius: 15px;
}

/*hide the outline behind the border*/
::-moz-focusring{
  outline: 1px solid white;
  outline-offset: -1px;
}

//Internet Explorer

::-ms-track {
  width: 100%;
  height: ${props => props.track_height}px;
  
  /*remove bg colour from the track, we'll use ms-fill-lower and ms-fill-upper instead */
  background: transparent;
  
  /*leave room for the larger thumb to overflow with a transparent border */
  border-color: transparent;
  border-width: 6px 0;

  /*remove default tick marks*/
  color: transparent;
}

::-ms-fill-lower {
  background: ${props => props.upper_color};
  border-radius: 15px;
}

::-ms-fill-upper {
  background: ${props => props.lower_color};
  border-radius: 15px;
}

::-ms-thumb {
  height: ${props => props.thumb_height};
  width: ${prop('thumb_height')};
  border-radius: 50%;
  background: ${props => props.thumb_color}; 
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.09);
  border: solid 2px #dbdbdb;
}

::-ms-tooltip {
  display: none;
}

${applyBreakpoints}
`;

const Slider = (props) => {
  return (

    <SliderStyle {...props} />

  );
};

Slider.propTypes = {
  thumb_height: PropTypes.string,
  thumb_color: PropTypes.string,
  lower_color: PropTypes.string,
  upper_color: PropTypes.string,
  track_height: PropTypes.number,
  value: PropTypes.string.isRequired
};

Slider.defaultProps = {
  height: '100%',
  thumb_height: '39px',
  thumb_color: '#ffffff',
  lower_color: '#9f2aff',
  upper_color: '#00baf7',
  track_height: 18

};

export { Slider };
export default Slider;
