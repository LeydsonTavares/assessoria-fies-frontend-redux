import React from "react";
import { Container } from "../../atoms";
import styled from "styled-components";
import PropTypes from "prop-types";
import { applyBreakpoints } from "../../../config/styles/mixins";
import * as mixins from "../../../config";

const HeaderType = props => <Container {...props} />;

const HeaderStyle = styled(HeaderType)`
  position: fixed;
  background: #59b7ae;
  z-index: 1;
  width: ${props => props.width};
  height: ${props => props.height};
  margin: ${props => props.margin && `${mixins.setSizes(props.margin)}`};
  min-width: ${props => props.minwidth};
  max-width: ${props => props.maxwidth};
  ${applyBreakpoints}
`;

const Header = props => {
  return <HeaderStyle {...props}>{props.children}</HeaderStyle>;
};

Header.propTypes = {
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  maxwidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  minwidth: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  margin: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.array,
    PropTypes.string
  ]),
  padding: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.array,
    PropTypes.string
  ])
};

Header.defaultProps = {
  width: "100%",
  heigth: "80px",
  margin: 0
};

export { Header };
export default Header;
