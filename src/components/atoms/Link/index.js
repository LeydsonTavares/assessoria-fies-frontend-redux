import React from 'react';
import styled from 'styled-components';
import { prop, ifProp } from 'styled-tools';
import PropTypes from 'prop-types';
import * as mixins from '../../../config';
import { Default as theme } from '../../../config/themes';
import { applyBreakpoints } from '../../../config/styles/mixins';

const LinkType = (props) => {
  switch (props.type) {
    case 'button':
      return <button type="button" {...props} />;
    case 'submit':
      return <button type="submit" {...props}>{props.children}</button>;
    case 'a':
    case 'anchor':
    default:
      return <a {...props}>{props.children}</a>;
  }
};

const LinkStyle = styled(LinkType)`
  display: ${prop('display')};

  border: 0;
  background: transparent;
  width: ${prop('width', 'initial')};
  height: ${prop('height', 'initial')};
  margin: ${props => props.margin && mixins.setSizes(props.margin)};
  padding: ${props => props.padding && mixins.setSizes(props.padding)};
  outline: 0;

  font-family: ${({ fontFamily }) => fontFamily || '\'Simplon\', sans-serif'};
  font-weight: ${({ fontWeight }) => mixins.fontWeight(fontWeight)};
  font-size: ${({ fontSize }) => mixins.rem(fontSize)};
  color: ${prop('color')};
  text-decoration: ${({ textDecoration }) => textDecoration};
  text-align: ${prop('align')};
  line-height: ${({ lineheight }) => (lineheight ? mixins.rem(lineheight) : '1em')};

  cursor: ${ifProp('cursor', prop('cursor'))};

  &:hover {
    text-decoration: underline;
  }

  ${applyBreakpoints}
`;

const Link = props => <LinkStyle {...props} />;

Link.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.string
  ]).isRequired,
  type: PropTypes.oneOf(['a', 'anchor', 'button', 'submit']).isRequired,
  display: PropTypes.oneOf(['block', 'inline-block', 'inline']),
  fontWeight: PropTypes.oneOf(['Light', 'Regular', 'Medium', 'Bold']),
  fontSize: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  color: PropTypes.string,
  align: PropTypes.oneOf(['center', 'left', 'right']),
  cursor: PropTypes.oneOf(['pointer', 'text']),
  textDecoration: PropTypes.string
};

Link.defaultProps = {
  display: 'block',
  fontWeight: 'Regular',
  fontSize: 16,
  color: theme.colors.primary,
  align: 'left',
  cursor: 'pointer',
  textDecoration: 'none'
};

export { Link };
export default Link;
