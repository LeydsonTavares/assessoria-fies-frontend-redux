import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { prop, ifProp, switchProp } from "styled-tools";
import * as m from "../../../config/styles/mixins";
import * as icons from "./iconfiles/index.js";
import { applyBreakpoints } from '../../../config/styles/mixins';


// Component Styles
const styles = css`
  color: #222222;
  box-sizing: border-box;
`;

/* eslint-disable */
const StyledIcon = styled.i`
  ${styles};
  display : ${props => props.display};
  position: ${({ position }) => position};
  top: ${({ top }) => (typeof top === "number" ? `${top}px` : top)};
  right: ${({ right }) => (typeof right === "number" ? `${right}px` : right)};
  bottom: ${({ bottom }) =>
    typeof bottom === "number" ? `${bottom}px` : bottom};
  left: ${({ left }) => (typeof left === "number" ? `${left}px` : left)};

  width: ${props =>
    props.width !== "auto" ? ` ${m.rem(props.width)}` : `${m.rem(props.size)}`};
  height: ${props =>
    props.height !== "auto"
      ? ` ${m.rem(props.height)}`
      : `${m.rem(props.size)}`};
  margin: ${props => props.margin && m.setSizes(props.margin)};

  transform: ${({ transform }) => transform};
  transition: all 0.3s ease-in-out;
  z-index: ${({ zindex }) => zindex};

  & svg {
    pointer-events: none;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  ${ifProp("autoSize", css`
      width: auto;
    `)}
  &.nofill svg{
    fill: #ffffff;
  }
  & svg:not(.notnone) :not([fill="none"]) :not([fill="#ffffff"]) {
    fill: ${prop("fillColor")};
    stroke: ${ifProp("strokeColor", prop("strokeColor"))};
    ${switchProp("themeColor", {
        primary: css`fill: ${prop("theme.colors.primary")};`,
        secondary: css`fill: ${prop("theme.colors.secondary")};`,
        success: css`fill: ${prop("theme.colors.success")};`,
        error: css`fill: ${prop("theme.colors.error")};`,
        warning: css`fill: ${prop("theme.colors.warning")};`,
        info: css`fill: ${prop("theme.colors.info")};`,
        light: css`fill: ${prop("theme.colors.light")};`,
        dark: css`fill: ${prop("theme.colors.dark")};`,
        link: css`fill: ${prop("theme.colors.link")};`
      })};
  }
  ${applyBreakpoints}

`;
/* eslint-enable */

const getIcon = props =>
  icons[props.type] ? (
    icons[props.type](props)
  ) : (
      icons['icon_default'](props)
    );

// Component Core
const Icon = props => <StyledIcon {...props}>{getIcon(props)}</StyledIcon>;

// Component Props
Icon.propTypes = {
  themeColor: PropTypes.oneOf([
    "primary",
    "secondary",
    "success",
    "error",
    "warning",
    "info",
    "light",
    "dark",
    "link",
    ""
  ]),
  type: PropTypes.string.isRequired,
  size: PropTypes.number,
  margin: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  fillColor: PropTypes.string,
  position: PropTypes.oneOf(["relative", "absolute", "fixed"]),
  top: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  right: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  bottom: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  left: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  transform: PropTypes.string,
  display: PropTypes.string,
  zindex: PropTypes.number
};

Icon.defaultProps = {
  size: 16,
  fillColor: "none",
  margin: [0],
  themeColor: "",
  width: "auto",
  height: "auto",
  transform: "initial",
  position: "relative",
  zindex: 0,
  display: 'inline-block'
};

export default Icon;
export { Icon };
