import React from 'react';

const netflix = (props) => {
    return (
        <svg width={props.size} height={props.size} viewBox="0 0 25 25" className="notnone" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <g id="Page-1" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <circle id="Oval" fill="#141414" cx="12.5" cy="12.5" r="12.5"></circle>
                <g transform="translate(7.000000, 3.000000)">
                    <g id="Group" fill="#D32F2F">
                        <rect id="Rectangle" x="7.33333333" y="0" width="3.66666667" height="19.5555556"></rect>
                        <rect id="Rectangle" x="0" y="0" width="3.66666667" height="19.5555556"></rect>
                    </g>
                    <polygon id="Path" fill="#F44336" points="11 19.5555556 7.33333333 19.5555556 0 0 3.66666667 0"></polygon>
                </g>
            </g>
        </svg>
    )
};

export {netflix};