import React from 'react';

const triangledown = function (props) {
    return (

        <svg
            version="1.1"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 640 640"
            width={40}
            height={40}
        >
            <defs>
                <path d="M74.01 203.13h312.33v293.54H74.01V203.13z" id="a" />
                <path
                    d="M381.56 333.92L454.28 242H163.37l72.72 91.92 72.74 91.92 72.73-91.92z"
                    id="b"
                />
            </defs>
            <use xlinkHref="#a" opacity={1} fill="#fff" fillOpacity={0} />
            <use xlinkHref="#b" opacity={1} fill="#d84c4c" fillOpacity={1} />
        </svg>
    );
};

export { triangledown };
export default triangledown;