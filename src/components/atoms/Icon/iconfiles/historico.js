import React from 'react';

const historico = function (props) {
    return (
        <svg width={props.width} height={props.width} className="notnone"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 32 27">
            <defs>
                <linearGradient
                    id="historico"
                    x1="100%"
                    x2="-36.312%"
                    y1="13.265%"
                    y2="117.83%"
                >
                    <stop offset="0%" stopColor="#d82482" />
                    <stop offset="100%" stopColor="#ffd700" />
                </linearGradient>
            </defs>
            <path
                fill="url(#historico)"
                fillRule="evenodd"
                d="M18 0C10.545 0 4.5 6.045 4.5 13.5H0l5.835 5.835.105.21L12 13.5H7.5C7.5 7.695 12.195 3 18 3s10.5 4.695 10.5 10.5S23.805 24 18 24c-2.895 0-5.52-1.185-7.41-3.09l-2.13 2.13A13.432 13.432 0 0018 27c7.455 0 13.5-6.045 13.5-13.5S25.455 0 18 0zm-1.5 7.5V15l6.42 3.81L24 16.995l-5.25-3.12V7.5H16.5z"
            />
        </svg>

    );

}

export { historico };
export default historico;