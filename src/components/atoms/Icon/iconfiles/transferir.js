import React from 'react';

const transferir = function (props) {
  return (
    <svg viewBox="0 0 32 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <defs>
        <path
          d="M11.216,19.4 L0,19.4 L0,22.6 L11.216,22.6 L11.216,27.4 L17.6,21 L11.216,14.6 L11.216,19.4 Z M20.784,17.8 L20.784,13 L32,13 L32,9.8 L20.784,9.8 L20.784,5 L14.4,11.4 L20.784,17.8 Z"
          id="path-transferir_icon"
        />
      </defs>
      <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="icone/setas/transferir" transform="translate(0.000000, -5.000000)">
          <mask id="mask-2" fill="white">
            <use xlinkHref="#path-transferir_icon" />
          </mask>
          <use id="Mask" fill="#222222" fillRule="nonzero" xlinkHref="#path-transferir_icon" />
        </g>
      </g>
    </svg>
  );
};

export {transferir};
export default transferir;
