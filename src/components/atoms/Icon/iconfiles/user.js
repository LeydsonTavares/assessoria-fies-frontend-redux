import React from 'react'

const user = function(props){

  

  return (
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
<defs>
    <path id="f" d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/>
</defs>
<g fill="none" fillRule="evenodd">
    <mask id="g" fill="#fff">
        <use href="#f"/>
    </mask>
    <use fill="#222" fillRule="nonzero" href="#f"/>
    <g fill="#222" mask="url(#g)">
        <path d="M0 0h24v24H0z"/>
    </g>
</g>
</svg>
  )
}

export {user}
export default user