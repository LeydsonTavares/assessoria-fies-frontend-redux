import React from 'react';

const codbar = function (props) {
    return (
        <svg width={props.width} height={props.width} {...props} className="notnone"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            viewBox="0 0 30 27"
        >
            <defs>
                <linearGradient id="a" x1="100%" x2="-36.312%" y1="9.5%" y2="124.783%">
                    <stop offset="0%" stopColor="#D82482" />
                    <stop offset="100%" stopColor="gold" />
                </linearGradient>
                <path
                    id="b"
                    d="M930 1936v-27h30v27h-30zm15.881-22.155h2.644v17.31h-2.644v-17.31zm-7.05 0h.882v17.31h-.881v-17.31zm-3.524 17.31v-17.31h1.762v17.31h-1.762zm7.05 0v-17.31h1.762v17.31h-1.763zm11.455 0v-17.31h.881v17.31h-.881zm-3.525 0v-17.31h1.763v17.31h-1.763zm-18.438 2.982h26.302v-23.274h-26.302v23.274z"
                />
            </defs>
            <use
                fill="url(#a)"
                fillRule="evenodd"
                transform="translate(-930 -1909)"
                xlinkHref="#b"
            />
        </svg>
    );
};

export { codbar };
export default codbar;
