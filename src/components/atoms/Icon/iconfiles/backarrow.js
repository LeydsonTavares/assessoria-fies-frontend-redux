import React from 'react';

const backarrow = (props) => {
  return (
    <svg viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <defs>
        <polygon id="path-backarrow" points="16 7 3.83 7 9.42 1.41 8 0 0 8 8 16 9.41 14.59 3.83 9 16 9" />
      </defs>
      <g id="VISUAL" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <mask id="mask-2" fill="white">
          <use xlinkHref="#path-backarrow" />
        </mask>
        <use id="Shape" fill="#000000" fillRule="nonzero" xlinkHref="#path-backarrow" />
      </g>
    </svg>
  );
};

export {backarrow};
export default backarrow;
