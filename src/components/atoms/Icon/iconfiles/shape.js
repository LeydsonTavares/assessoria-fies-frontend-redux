
import React from 'react'

const shape = (props) => (

    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51 63" width={props.width} height={props.width}
        className="notnone">
        <defs>
            <linearGradient id="shape" x1="82.766%" x2="-6.562%" y1="0%" y2="142.324%">
                <stop offset="0%" stopColor="#D82482" />
                <stop offset="100%" stopColor="gold" />
            </linearGradient>
        </defs>
        <path
            fill="url(#shape)"
            fillRule="evenodd"
            d="M513.364 885.439h27.818v21.581l-13.91 7.614-13.908-7.614v-21.58zm3.974 19.44l9.935 5.438 9.935-5.439v-15.71h-19.87v15.71zM552 873.438v42.892L526.501 930 501 916.362v-42.923h17.909L526.5 867l7.591 6.44H552zm-32.365 0h-.726l-.55.467 1.276-.467zm15.007.467l-.551-.467h-.726l1.277.467zm-2.002 3.416l-6.14-5.209-6.14 5.209h-15.437v36.725l21.576 11.539 21.578-11.567v-36.697H532.64z"
            transform="translate(-501 -867)"
        />
    </svg>
)

export { shape }
export default shape