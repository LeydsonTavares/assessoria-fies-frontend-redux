import React from "react";

const oval = function (props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 37 37"
      width={props.width} height={props.width}
      className="notnone"
    >
      <defs>
        <linearGradient id="b" x1="100%" x2="0%" y1="50%" y2="50%">
          <stop offset="0%" stopColor="#FE6B03" />
          <stop offset="100%" stopColor="#E33465" />
        </linearGradient>
        <circle id="a" cx="18.5" cy="18.5" r="18.5" />
        <path
          id="c"
          d="M15.5275 15L20.5 19.3265857 25.4725 15 27 16.3319838 20.5 22 14 16.3319838z"
        />
      </defs>
      <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
        <mask fill="#fff">
          <use xlinkHref="#a" />
        </mask>
        <use fill="url(#b)" xlinkHref="#a" />
        <mask fill="#fff">
          <use xlinkHref="#c" />
        </mask>
        <use
          fill="#FFF"
          fillRule="nonzero"
          transform="rotate(-90 20.5 18.5)"
          xlinkHref="#c"
        />
      </g>
    </svg>
  );
}

export { oval };
export default oval;
