import React from 'react';

const file = function (props) {
    return (
        <svg width={props.width} height={props.width} {...props} className="notnone"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 25 31">
            <defs>
                <linearGradient id="file" x1="82.183%" x2="-5.555%" y1="0%" y2="142.324%">
                    <stop offset="0%" stopColor="#d82482" />
                    <stop offset="100%" stopColor="#ffd700" />
                </linearGradient>
            </defs>
            <path
                fill="url(#file)"
                fillRule="evenodd"
                d="M22.57 10.92h-7.292a1.05 1.05 0 01-1.042-1.056V2.466H4.166c-.948 0-1.722.79-1.722 1.762l-.013 22.545c0 .97.774 1.761 1.722 1.761h16.68c.953 0 1.736-.795 1.736-1.761V10.92zM15.277.352c.276 0 .541.112.736.31l8.334 8.454c.195.199.305.467.305.748v16.909c0 2.133-1.717 3.875-3.82 3.875H4.153c-2.102 0-3.806-1.74-3.806-3.876L.361 4.227C.361 2.093 2.065.352 4.167.352h11.11zm1.041 8.455h4.083l-4.083-4.142v4.142zM6.944 15.5h11.112v2.114H6.944V15.5zm.695.705l-.347.352.347.352h.694v-.704H7.64zm-.695 4.931h11.112v2.114H6.944v-2.114zm.695.705l-.347.352.347.352h.694v-.704H7.64z"
            />
        </svg>
    );

}

export { file };
export default file;