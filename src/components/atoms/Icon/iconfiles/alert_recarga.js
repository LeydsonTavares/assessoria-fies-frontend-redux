import React from 'react';

const alert_recarga = () => (
<svg width='100%' height='100%' xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24">
    <g fillRule="evenodd">
        <path id="a" d="M12 0c6.617 0 12 5.383 12 12s-5.383 12-12 12S0 18.617 0 12 5.383 0 12 0zM2.389 12c0 5.3 4.311 9.611 9.611 9.611 5.3 0 9.611-4.311 9.611-9.611 0-5.3-4.311-9.611-9.611-9.611C6.7 2.389 2.389 6.7 2.389 12zm8.877 1.999V4.467h1.846v9.532h-1.846zm0 5v-2.532h1.846v2.532h-1.846z"/>
    </g>
</svg>
);

export { alert_recarga };
export default alert_recarga;
