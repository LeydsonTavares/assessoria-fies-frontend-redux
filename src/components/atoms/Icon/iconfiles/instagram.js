import React from 'react';

const instagram = function (props) {
    return (
        <svg
            width={props.size} height={props.size}
            xmlnsXlink="http://www.w3.org/1999/xlink"
            viewBox="0 0 25 25"
            version="1.1"
        >
            <defs>
                <linearGradient
                    x1="21.3192187%"
                    y1="90.9614844%"
                    x2="78.6816406%"
                    y2="9.039375%"
                    id="a"
                >
                    <stop stopColor="#FFB900" offset="0%" />
                    <stop stopColor="#9100EB" offset="100%" />
                </linearGradient>
            </defs>
            <g
                transform="translate(-29.000000, -356.000000) translate(29.000000, 356.000000)"
                stroke="none"
                strokeWidth={1}
                fill="none"
                fillRule="evenodd"
            >
                <circle fill="url(#a)" cx="12.5" cy="12.5" r="12.5" />
                <g transform="translate(4.687500, 4.687500)" fill="#FFF">
                    <path
                        d="M11.393 15.625H4.232A4.237 4.237 0 0 1 0 11.393V4.232A4.237 4.237 0 0 1 4.232 0h7.161a4.236 4.236 0 0 1 4.232 4.232v7.161a4.236 4.236 0 0 1-4.232 4.232zM4.232 1.302a2.933 2.933 0 0 0-2.93 2.93v7.161a2.933 2.933 0 0 0 2.93 2.93h7.161a2.933 2.933 0 0 0 2.93-2.93V4.232a2.933 2.933 0 0 0-2.93-2.93H4.232z"
                        fillRule="nonzero" fill="#ffffff"
                    />
                    <path
                        d="M7.813 11.719a3.91 3.91 0 0 1-3.907-3.906 3.91 3.91 0 0 1 3.906-3.907 3.91 3.91 0 0 1 3.907 3.906 3.91 3.91 0 0 1-3.906 3.907zm0-6.51a2.607 2.607 0 0 0-2.605 2.604 2.607 2.607 0 0 0 2.604 2.604 2.607 2.607 0 0 0 2.605-2.604 2.607 2.607 0 0 0-2.604-2.605z"
                        fillRule="nonzero" fill="#ffffff"
                    />
                    <circle cx="11.9628906" cy="3.66210938"  fillRule="nonzero" fill="#ffffff" r={1} />
                </g>
            </g>
        </svg>
    );

}

export { instagram };
export default instagram;