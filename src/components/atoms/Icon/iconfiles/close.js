import React from 'react';

const close = function (props) {
  return (
    <svg
      viewBox="-639.5 400.5 27 27"
      xmlns="http://www.w3.org/2000/svg" fill={props.fillColor}>
      <g>
        <path
          d="M-612.5,401.9l-1.4-1.4c0,0,0,0-12.1,12.1c-12.1-12.1-12.1-12.1-12.1-12.1l-1.4,1.4l12.1,12.1l-12.1,12.1l1.4,1.4 c0,0,0,0,12.1-12.1c12.1,12.1,12.1,12.1,12.1,12.1l1.4-1.4l-12.1-12.1L-612.5,401.9z"
          fillRule="none"
        />
      </g>
    </svg>
  );
};

export {close};
export default close;
