import React from 'react'

const icon_default = function (props) {
    return (
        <svg version="1.1" baseProfile="full" width={props.size + 6} height={props.size + 6}>
            <circle cx={props.cx} cy={props.cy} r={props.r} fill="grey" />
        </svg>)
}

export { icon_default }
export default icon_default