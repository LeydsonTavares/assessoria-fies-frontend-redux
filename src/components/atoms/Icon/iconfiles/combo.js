import React from 'react'

const combo = function(props){

  

  return (
    <svg
                viewBox="-430 236 99 90"
                xmlns="http://www.w3.org/2000/svg">
                <g>
                    <polygon 
                    id="Combo-Shape-Copy-6" 
                    points="-397.1,236 -397.1,239.4 -426.6,239.4 -426.6,322.6 -397.1,322.6 -397.1,326 -430,326 -430,236 		"
                    />
                    <polygon 
                    id="Combo-Shape-Copy-7" 
                    points="-364,236 -364,239.4 -334.4,239.4 -334.4,322.6 -364,322.6 -364,326 -331.1,326 -331.1,236"
                    />
                </g>
    </svg>
  )
}

export {combo}
export default combo

