import React from 'react';

const plus_sign = function (props) {
  return (
    <svg viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <defs>
        <path
          d="M28.3720301,12.9069569 L17.0930431,12.9069569 L17.0930431,1.62790887 C17.0930431,1.11410204 16.3953417,0 15,0 C13.6046583,0 12.9069569,1.11416303 12.9069569,1.62790887 L12.9069569,12.9070179 L1.62790887,12.9070179 C1.11416303,12.9069569 0,13.6046583 0,14.999939 C0,16.3952198 1.11416303,17.0929821 1.62790887,17.0929821 L12.9070179,17.0929821 L12.9070179,28.3720911 C12.9070179,28.885776 13.6046583,30 15.000061,30 C16.3954637,30 17.0931041,28.885776 17.0931041,28.3720911 L17.0931041,17.0929821 L28.3722131,17.0929821 C28.885898,17.0929821 30.000122,16.3953417 30.000122,14.999939 C30.000122,13.6045363 28.885776,12.9069569 28.3720301,12.9069569 Z"
          id="path-plus_sign_icon"
        />
      </defs>
      <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="icone/navegacao/adicionar-2" transform="translate(-1.000000, -1.000000)">
          <g id="icone/navegacao/buscar-2" transform="translate(1.000000, 1.000000)">
            <mask id="mask-2" fill="white">
              <use xlinkHref="#path-plus_sign_icon" />
            </mask>
            <use id="Shape" fill="#000000" fillRule="nonzero" xlinkHref="#path-plus_sign_icon" />
          </g>
        </g>
      </g>
    </svg>
  );
};

export {plus_sign};
export default plus_sign;
