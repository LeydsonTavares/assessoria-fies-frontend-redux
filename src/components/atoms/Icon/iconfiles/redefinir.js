import React from 'react';

const redefinir = function (props) {
  return (
    <svg viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <defs>
        <path
          d="M0,24.8888889 L0,28.4444444 L10.6666667,28.4444444 L10.6666667,24.8888889 L0,24.8888889 Z M0,3.55555556 L0,7.11111111 L17.7777778,7.11111111 L17.7777778,3.55555556 L0,3.55555556 Z M17.7777778,32 L17.7777778,28.4444444 L32,28.4444444 L32,24.8888889 L17.7777778,24.8888889 L17.7777778,21.3333333 L14.2222222,21.3333333 L14.2222222,32 L17.7777778,32 Z M7.11111111,10.6666667 L7.11111111,14.2222222 L0,14.2222222 L0,17.7777778 L7.11111111,17.7777778 L7.11111111,21.3333333 L10.6666667,21.3333333 L10.6666667,10.6666667 L7.11111111,10.6666667 Z M32,17.7777778 L32,14.2222222 L14.2222222,14.2222222 L14.2222222,17.7777778 L32,17.7777778 Z M21.3333333,10.6666667 L24.8888889,10.6666667 L24.8888889,7.11111111 L32,7.11111111 L32,3.55555556 L24.8888889,3.55555556 L24.8888889,0 L21.3333333,0 L21.3333333,10.6666667 Z" // eslint-disable-line
          id="pathRedefinir_icon"
        />
      </defs>
      <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="icone/navegacao/ajustar">
          <mask id="mask-2" fill="white">
            <use xlinkHref="#pathRedefinir_icon" />
          </mask>
          <use id="Mask" fill="#222222" fillRule="nonzero" xlinkHref="#pathRedefinir_icon" />
        </g>
      </g>
    </svg>
  );
};

export {redefinir};
export default redefinir;
