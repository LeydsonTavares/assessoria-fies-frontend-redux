import React from 'react'

const youtube = function (props) {

  return (
    <svg
      width={props.size} height={props.size}
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 25 25"
      version="1.1"
    >
      <defs>
        <linearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="b">
          <stop stopColor="#E52D27" offset="0%" />
          <stop stopColor="#BF171D" offset="100%" />
        </linearGradient>
      </defs>
      <g
        transform="translate(-161.000000, -356.000000) translate(161.000000, 356.000000)"
        stroke="none"
        strokeWidth={1}
        fill="none"
        fillRule="evenodd"
      >
        <path
          d="M0 12.5C0 5.594 5.594 0 12.5 0S25 5.594 25 12.5 19.406 25 12.5 25 0 19.406 0 12.5"
          fill="url(#b)"
        />
        <g transform="translate(4.000000, 6.500000)">
          <polygon
            fill="#000"
            opacity="0.12"
            points="6.6 3.43125 10.59375 6.1 11.15625 5.8125"
          />
          <path
            d="M16.7 2.569s-.162-1.163-.669-1.675C15.394.219 14.67.219 14.344.18c-2.363-.168-5.9-.168-5.9-.168h-.007s-3.537 0-5.9.168C2.207.22 1.487.225.85.894.344 1.406.181 2.569.181 2.569S.013 3.93.013 5.3v1.281c0 1.363.168 2.731.168 2.731s.163 1.163.669 1.676c.644.675 1.481.65 1.856.718 1.35.132 5.732.169 5.732.169s3.543-.006 5.906-.175c.331-.037 1.05-.044 1.687-.712.507-.513.669-1.675.669-1.675s.169-1.363.169-2.732V5.3c0-1.369-.169-2.731-.169-2.731zM6.694 8.125V3.388l4.556 2.375-4.556 2.362z"
            fillRule="nonzero" fill="#ffffff"
          />
        </g>
      </g>
    </svg>
  )
}

export { youtube }
export default youtube