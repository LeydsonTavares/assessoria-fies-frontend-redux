import React from 'react';

const check = props => (
  <svg {...props}
    className="notnone">
    <defs>
      <linearGradient
        id="prefix__b"
        x1="353.937%"
        x2="-37.517%"
        y1="105.026%"
        y2="105.026%"
      >
        <stop offset="0%" stopColor="#FFFF00" />
        <stop offset="41.941%" stopColor="#FF6D00" />
        <stop offset="100%" stopColor="#EA288C" />
      </linearGradient>
      <path
        id="prefix__a"
        d="M2.05 4.198l3.768 3.63L13.95.5 16 2.376 5.818 11.593 0 6.074z"
      />
    </defs>
    <g fill="none" fillRule="evenodd">
      <mask id="prefix__c" fill="#fff">
        <use xlinkHref="#prefix__a" />
      </mask>
      <use fill="#000" fillRule="nonzero" xlinkHref="#prefix__a" />
      <g fill="url(#prefix__b)" mask="url(#prefix__c)">
        <path d="M0-2h16v16H0z" />
      </g>
    </g>
  </svg>
)


export { check };
export default check;
