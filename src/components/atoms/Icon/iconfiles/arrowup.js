import React from 'react';

const arrowup = function (props) {
  return (
    <svg viewBox="0 0 16 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
      <defs>
        <polygon id="path-arrowup" points="1.88 0 8 6.59289249 14.12 0 16 2.02968961 8 10.6666667 0 2.02968961" />
      </defs>
      <g id="VISUAL" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <mask id="mask-2" fill="white">
          <use xlinkHref="#path-arrowup" />
        </mask>
        <use id="Seta-expandir" fill="#222222" fillRule="nonzero" transform="translate(8.000000, 5.333333) scale(1, -1) translate(-8.000000, -5.333333) " xlinkHref="#path-arrowup" />
      </g>
    </svg>
  );
};

export {arrowup};
export default arrowup;
