import React from 'react'
import Text from "../../atoms/Text/index.js";
import Title from "../../atoms/Title";
import Container from '../Container/index.js';


const Validade = (props) => {
    return (
        <Container margin={props.margin}>
            <Text fontcolor='#000000' fontSize={12}>{props.titulo}</Text>
            <Text fontcolor='#909090' fontSize={11}>{props.texto}</Text>
        </Container>
    )
}


export default Validade