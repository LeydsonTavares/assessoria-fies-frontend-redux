import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import * as mixins from "../../../config";
import { applyBreakpoints } from "../../../config/styles/mixins";

const CheckboxStyle = styled.label`
  .container {
    display: flex;
    cursor: pointer;
    font-size: 22px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    height: ${props => props.height};
  }

  /* Hide the browser's default checkbox */
  .container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  /* Create a custom checkbox */
  .checkmark {
    position: absolute;
    margin: ${props => props.margin && mixins.setSizes(props.margin)};
    top: 0;
    left: 0;
    height: ${props => props.height};
    width: ${props => props.width};
    background-color: white;
    border: ${props => props.border};
    border-radius: ${props => props.radius};
  }

  /* On mouse-over, add a grey background color */
  .container:hover input ~ .checkmark {
    background-color: white;
  }

  /* When the checkbox is checked, add a purple background */
  .container input:checked ~ .checkmark {
    background-color: ${props => props.color};
    border-color: ${props => props.color};
  }

  /* Create the checkmark/indicator (hidden when not checked) */
  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }

  /* Show the checkmark when checked */
  .container input:checked ~ .checkmark:after {
    display: block;
  }

  /* Style the checkmark/indicator */
  .container .checkmark:after {
    width: ${props => props.checkWidth};
    height: ${props => props.checkHeight};
    top: ${props => props.checkTop};
    left: ${props => props.checkLeft};
    right: ${props => props.checkRight};
    bottom: ${props => props.checkBottom};
    border: solid white;
    border-width: ${props => props.borderWidth};
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }

  ${applyBreakpoints};
`;

const Checkbox = props => {
  return (
    <CheckboxStyle {...props}>
      <label className="container" {...props}>
        <input
          type="checkbox"
          data-context={props.dataContext}
          checked={props.checked}
          onClick={props.onClick}
        ></input>
        <span className="checkmark"></span>
        {props.children}
      </label>
    </CheckboxStyle>
  );
};

Checkbox.propTypes = {
  height: PropTypes.string,
  width: PropTypes.string,
  color: PropTypes.string,
  checkWidth: PropTypes.string,
  checkHeight: PropTypes.string,
  radius: PropTypes.string,
  margin: PropTypes.oneOfType([PropTypes.number, PropTypes.array]),
  borderWidth: PropTypes.string,
  border: PropTypes.string
};

Checkbox.defaultProps = {
  height: "18px",
  width: "18px",
  color: "#d82482",
  checkWidth: "4px",
  checkHeight: "10px",
  radius: "2px",
  checkTop: "0px",
  checkRight: "0px",
  checkBottom: "0px",
  checkLeft: "3px",
  margin: [0],
  borderWidth: "0 1px 1px 0",
  border: "1px solid black"
};

export { Checkbox };
export default Checkbox;
