import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { IconButton, Grid } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { Line } from "../../atoms";

const useStyles = makeStyles({
  root: {
    padding: "2%",
    textAlign: "left",
  },
  title: {
    fontSize: 25,
  },
  pos: {
    marginBottom: 12,
  },
});

const CardOpCurso = (props) => {
  const opcao = props.opcao;
  const classes = useStyles();

  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography
          className={classes.title}
          color="textSecondary"
          gutterBottom
        >
          {`Opção de Curso ${props.id + 1}`}
          <Grid item xs={12}>
            <Line width={"100%"} color={"#ccc"}></Line>
          </Grid>
        </Typography>
        <Typography>{`CURSO: ${opcao.curso}`}</Typography>
        <Typography>{opcao.municipio + " - " + opcao.estado}</Typography>
        <Typography>{`INSTITUIÇÃO: ${opcao.ies}`}</Typography>
        <Typography>{`LOCAL: ${opcao.local}`}</Typography>
        <Grid item xs={12}>
          <Line width={"100%"} color={"#ccc"}></Line>
        </Grid>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="add to favorites">
          <EditIcon />
        </IconButton>
        <IconButton aria-label="add to favorites">
          <DeleteIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export { CardOpCurso };
export default CardOpCurso;
