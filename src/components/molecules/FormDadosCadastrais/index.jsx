import React, { useState, useEffect } from "react";
import { Paper, makeStyles } from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import FormFormik from "./formFormik";
import {
  getEstadoCivil,
  getRedePublica,
  getRacaCor,
} from "../../../containers/pages/Dashboard/service";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(() => ({
  card: {
    padding: "2%",
  },
  buttonNext: {
    backgroundColor: "#fd970f",
    "&:hover": {
      backgroundColor: "#008181",
    },
    width: "100px",
  },
  buttonBack: {
    backgroundColor: "#008181",
    "&:hover": {
      backgroundColor: "#fd970f",
    },
    marginRight: "24px",
    width: "100px",
  },
}));

const FormDadosCadastrais = (props) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [estadoCivil, setEstadoCivil] = useState([]);
  const [redePublica, setRedePublica] = useState([]);
  const [categorizacaoHumanaBrsaileira, setRacaCor] = useState([]);

  const _getEstadoCivil = () => {
    getEstadoCivil().then((response) => {
      setEstadoCivil(response.data);
    });
  };
  const _getRacaCor = () => {
    getRacaCor().then((response) => {
      setRacaCor(response.data);
    });
  };
  const _getRedePublica = () => {
    getRedePublica().then((response) => {
      setRedePublica(response.data);
    });
  };

  useEffect(() => {
    _getEstadoCivil();
    _getRedePublica();
    _getRacaCor();
  }, []);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <Paper className={classes.card} elevation={0}>
      <FormFormik
        onClickNext={props.onClickNext}
        openSucesso={handleClick}
        estadoCivil={estadoCivil}
        redePublica={redePublica}
        categorizacaoHumanaBrsaileira={categorizacaoHumanaBrsaileira}
      ></FormFormik>
      <Snackbar open={open} autoHideDuration={2000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          * As informações foram gravadas com sucesso
        </Alert>
      </Snackbar>
    </Paper>
  );
};

export { FormDadosCadastrais };
export default FormDadosCadastrais;
