import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { withFormik } from "formik";
import { Persist } from "formik-persist";
import * as Yup from "yup";
import {
  Paper,
  Grid,
  FormControlLabel,
  RadioGroup,
  Card,
  FormLabel,
  Radio,
  FormHelperText,
  CircularProgress,
  MenuItem,
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { Line } from "../../atoms";
import DateFnsUtils from "@date-io/date-fns";
import Autocomplete from "@material-ui/lab/Autocomplete";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import Button from "@material-ui/core/Button";
import {
  cellphoneMask,
  cpfMask,
  cepMask,
  intMask,
} from "../../../helpers/_mask";
import { SIM, NAO } from "../../../helpers/_constants";
import { getEstado, genero } from "../../../helpers/_dataJson";

const styles = () => ({
  padding: {
    padding: "2%",
  },
  textAlignLeft: {
    textAlign: "left",
  },
  borderErro: {
    border: "1px solid #f44336",
  },
  marginTop: {
    marginTop: "24px",
  },
  direction: {
    display: "flex",
    flexDirection: "row",
  },
  bottom: {
    color: "#008181",
    animationDuration: "550ms",
    position: "absolute",
  },
  marginHelperText: {
    marginLeft: "14px",
    marginRight: "14px",
  },
  buttonNext: {
    backgroundColor: "#fd970f",
    "&:hover": {
      backgroundColor: "#008181",
    },
    width: "100px",
    height: "36px",
  },
});

const form = (props) => {
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = props;

  const onFocusFieldErro = () => {
    if (JSON.stringify(errors) !== "{}") {
      document.getElementById(Object.keys(errors)[0]).focus();
    }
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <form onSubmit={handleSubmit}>
        <Paper className={classes.padding} elevation={0}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={8}>
              <TextField
                onChange={handleChange}
                margin="dense"
                onBlur={handleBlur}
                helperText={touched.nome ? errors.nome : ""}
                error={touched.nome && Boolean(errors.nome)}
                id="nome"
                label="Nome completo"
                type="text"
                value={values.nome}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange("cpf")}
                onBlur={handleBlur}
                helperText={touched.cpf ? errors.cpf : ""}
                error={touched.cpf && Boolean(errors.cpf)}
                id="cpf"
                margin="dense"
                label="CPF"
                inputProps={{ maxLength: 14 }}
                type="text"
                value={cpfMask(values.cpf)}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <KeyboardDatePicker
                autoOk
                style={{ width: "100%" }}
                margin="dense"
                variant="inline"
                id="dt_nascimento"
                inputVariant="outlined"
                label="Data de nascimento *"
                format="dd/MM/yyyy"
                value={values.dt_nascimento}
                onBlur={handleBlur}
                helperText={touched.dt_nascimento ? errors.dt_nascimento : ""}
                error={touched.dt_nascimento && Boolean(errors.dt_nascimento)}
                selected={values.dt_nascimento}
                onChange={(date) => setFieldValue("dt_nascimento", date)}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                select
                className={classes.textAlignLeft}
                id="genero"
                label="Gênero"
                value={values.genero}
                onChange={handleChange("genero")}
                helperText={touched.genero ? errors.genero : ""}
                error={touched.genero && Boolean(errors.genero)}
                margin="dense"
                variant="outlined"
                fullWidth
              >
                {genero.map((option) => (
                  <MenuItem key={option.id} value={option.value}>
                    {option.value}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                select
                className={classes.textAlignLeft}
                id="estado_civil"
                label="Estado civil"
                value={values.estado_civil}
                onChange={handleChange("estado_civil")}
                helperText={touched.estado_civil ? errors.estado_civil : ""}
                error={touched.estado_civil && Boolean(errors.estado_civil)}
                margin="dense"
                variant="outlined"
                fullWidth
              >
                {props.estadoCivil.map((option) => (
                  <MenuItem key={option.id} value={option.id}>
                    {option.descricao}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange}
                onBlur={handleBlur}
                margin="dense"
                helperText={touched.rg ? errors.rg : ""}
                error={touched.rg && Boolean(errors.rg)}
                id="rg"
                value={values.rg}
                label="RG"
                type="number"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange}
                value={values.orgao_emissor}
                onBlur={handleBlur}
                margin="dense"
                helperText={touched.orgao_emissor ? errors.orgao_emissor : ""}
                error={touched.orgao_emissor && Boolean(errors.orgao_emissor)}
                id="orgao_emissor"
                label="Órgão emissor"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <KeyboardDatePicker
                autoOk
                style={{ width: "100%" }}
                variant="inline"
                id="dt_emissao"
                margin="dense"
                inputVariant="outlined"
                label="Data de emissão *"
                format="dd/MM/yyyy"
                value={values.dt_emissao}
                error={touched.dt_emissao && Boolean(errors.dt_emissao)}
                helperText={touched.dt_emissao ? errors.dt_emissao : ""}
                selected={values.dt_emissao}
                onBlur={handleBlur}
                onChange={(date) => setFieldValue("dt_emissao", date)}
              />
            </Grid>
            <Grid item xs={12}>
              <Line width={"100%"} color={"#ccc"}></Line>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                type="email"
                margin="dense"
                value={values.email}
                onChange={handleChange("email")}
                onBlur={handleBlur}
                helperText={touched.email ? errors.email : ""}
                error={touched.email && Boolean(errors.email)}
                id="email"
                label="Email"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange("tel_residencial")}
                onBlur={handleBlur}
                helperText={
                  touched.tel_residencial ? errors.tel_residencial : ""
                }
                error={
                  touched.tel_residencial && Boolean(errors.tel_residencial)
                }
                value={cellphoneMask(values.tel_residencial)}
                margin="dense"
                id="tel_residencial"
                label="Telefone residencial"
                type="text"
                inputProps={{ maxLength: 14 }}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                value={cellphoneMask(values.tel_celular)}
                onChange={handleChange("tel_celular")}
                margin="dense"
                onBlur={handleBlur}
                helperText={touched.tel_celular ? errors.tel_celular : ""}
                error={touched.tel_celular && Boolean(errors.tel_celular)}
                id="tel_celular"
                label="Telefone celular"
                type="text"
                inputProps={{ maxLength: 15 }}
                fullWidth
                variant="outlined"
              ></TextField>
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange("cep")}
                autoFocus={errors.cep ? true : false}
                onBlur={handleBlur}
                helperText={touched.cep ? errors.cep : ""}
                error={touched.cep && Boolean(errors.cep)}
                margin="dense"
                id="cep"
                label="CEP"
                type="text"
                value={cepMask(values.cep)}
                inputProps={{ maxLength: 9 }}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={8}>
              <TextField
                id="endereco"
                label="Endereço"
                value={values.endereco}
                onChange={handleChange("endereco")}
                margin="dense"
                onBlur={handleBlur}
                helperText={touched.endereco ? errors.endereco : ""}
                error={touched.endereco && Boolean(errors.endereco)}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={touched.numero ? errors.numero : ""}
                error={touched.numero && Boolean(errors.numero)}
                margin="dense"
                id="numero"
                label="Número"
                type="text"
                value={intMask(values.numero)}
                inputProps={{ maxLength: 7 }}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={8}>
              <TextField
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={touched.complemento ? errors.complemento : ""}
                error={touched.complemento && Boolean(errors.complemento)}
                margin="dense"
                value={values.complemento}
                id="complemento"
                label="Complemento"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange}
                onBlur={handleBlur}
                helperText={touched.bairro ? errors.bairro : ""}
                error={touched.bairro && Boolean(errors.bairro)}
                value={values.bairro}
                id="bairro"
                margin="dense"
                label="Bairro"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Autocomplete
                id="uf"
                options={getEstado}
                getOptionLabel={(option) => option.nome}
                onChange={(e, value) => {
                  setFieldValue("uf", value);
                }}
                value={values.uf}
                onBlur={handleBlur}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    fullWidth
                    label="Estado"
                    margin="dense"
                    variant="outlined"
                    helperText={touched.uf ? errors.uf : ""}
                    error={touched.uf && Boolean(errors.uf)}
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <TextField
                onChange={handleChange("municipio")}
                onBlur={handleBlur}
                helperText={touched.municipio ? errors.municipio : ""}
                error={touched.municipio && Boolean(errors.municipio)}
                value={values.municipio}
                id="municipio"
                label="Município"
                margin="dense"
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12}>
              <Line width={"100%"} color={"#ccc"}></Line>
            </Grid>
            <Grid container item xs={12} sm={4}>
              <TextField
                select
                className={classes.textAlignLeft}
                id="raca_cor"
                label="Raça/Cor"
                value={values.raca_cor}
                onChange={handleChange("raca_cor")}
                helperText={touched.raca_cor ? errors.raca_cor : ""}
                error={touched.raca_cor && Boolean(errors.raca_cor)}
                margin="dense"
                variant="outlined"
                fullWidth
              >
                {props.categorizacaoHumanaBrsaileira.map((option) => (
                  <MenuItem key={option.id} value={option.id}>
                    {option.value}
                  </MenuItem>
                ))}
              </TextField>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                item
                className={classes.marginTop}
                xs={12}
              >
                <FormLabel
                  id="deficiencia"
                  className={classes.textAlignLeft}
                  component="legend"
                >
                  Pessoa com deficiência? *
                  <FormHelperText
                    className={classes.marginHelperText}
                    error={touched.deficiencia && Boolean(errors.deficiencia)}
                  >
                    {touched.deficiencia ? errors.deficiencia : ""}
                  </FormHelperText>
                </FormLabel>

                <RadioGroup
                  onChange={handleChange("deficiencia")}
                  id="deficiencia"
                  value={values.deficiencia}
                  error={errors.deficiencia}
                  className={classes.direction}
                >
                  <FormControlLabel
                    className={classes.textAlignLeft}
                    value={SIM}
                    control={<Radio color="default" />}
                    label="Sim"
                  />
                  <FormControlLabel
                    className={classes.textAlignLeft}
                    value={NAO}
                    control={<Radio color="default" />}
                    label="Não"
                  />
                </RadioGroup>

                <FormLabel
                  id="conclusao_curso_supeiror"
                  className={classes.textAlignLeft}
                  component="legend"
                >
                  Concluiu curso superior? *
                  <FormHelperText
                    className={classes.marginHelperText}
                    error={
                      touched.conclusao_curso_supeiror &&
                      Boolean(errors.conclusao_curso_supeiror)
                    }
                  >
                    {touched.conclusao_curso_supeiror
                      ? errors.conclusao_curso_supeiror
                      : ""}
                  </FormHelperText>
                </FormLabel>
                <RadioGroup
                  onChange={handleChange("conclusao_curso_supeiror")}
                  id="conclusao_curso_supeiror"
                  value={values.conclusao_curso_supeiror}
                  error={errors.conclusao_curso_supeiror}
                  className={classes.direction}
                >
                  <FormControlLabel
                    className={classes.textAlignLeft}
                    value={SIM}
                    control={<Radio color="default" />}
                    label="Sim"
                  />
                  <FormControlLabel
                    className={classes.textAlignLeft}
                    value={NAO}
                    control={<Radio color="default" />}
                    label="Não"
                  />
                </RadioGroup>
              </Grid>
              <TextField
                onChange={handleChange("ano_conclusao")}
                value={intMask(values.ano_conclusao)}
                margin="dense"
                onBlur={handleBlur}
                helperText={touched.ano_conclusao ? errors.ano_conclusao : ""}
                error={touched.ano_conclusao && Boolean(errors.ano_conclusao)}
                id="ano_conclusao"
                label="Ano conclusão *"
                type="text"
                inputProps={{ maxLength: 4 }}
                fullWidth
                variant="outlined"
              />
            </Grid>
            <Grid item xs={12} sm={8}>
              <Card
                className={[
                  classes.padding,
                  touched.rede_publica && errors.rede_publica
                    ? classes.borderErro
                    : "",
                ].join(" ")}
                variant="outlined"
              >
                <FormLabel
                  id="conclusao_curso_supeiror"
                  className={classes.textAlignLeft}
                  component="legend"
                >
                  Cursou o ensino médio em escola da rede pública?
                </FormLabel>
                <RadioGroup
                  onChange={handleChange("rede_publica")}
                  id="rede_publica"
                  type="number"
                  value={Number.parseInt(values.rede_publica)}
                  error={errors.rede_publica}
                >
                  {props.redePublica.map((item, index) => {
                    return (
                      <FormControlLabel
                        className={classes.textAlignLeft}
                        key={index}
                        value={Number.parseInt(item.id)}
                        control={<Radio color="default" />}
                        label={item.descricao}
                      />
                    );
                  })}
                </RadioGroup>
              </Card>
              <FormHelperText
                className={classes.marginHelperText}
                error={touched.rede_publica && Boolean(errors.rede_publica)}
              >
                {touched.rede_publica ? errors.rede_publica : ""}
              </FormHelperText>
            </Grid>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
              item
              xs={12}
            >
              <Button
                className={classes.buttonNext}
                variant="contained"
                color="primary"
                type="submit"
                disabled={isSubmitting}
                onClick={() => onFocusFieldErro()}
              >
                {isSubmitting ? (
                  <CircularProgress
                    variant="indeterminate"
                    disableShrink
                    className={classes.bottom}
                    size={24}
                    thickness={4}
                    {...props}
                  />
                ) : (
                  "Avançar"
                )}
              </Button>
            </Grid>
          </Grid>
        </Paper>
        <Persist name="form-passo1" />
      </form>
    </MuiPickersUtilsProvider>
  );
};

const FormFormik = withFormik({
  mapPropsToValues: ({
    nome,
    cpf,
    rg,
    dt_nascimento,
    genero,
    orgao_emissor,
    dt_emissao,
    estado_civil,
    email,
    tel_residencial,
    tel_celular,
    cep,
    endereco,
    numero,
    complemento,
    bairro,
    uf,
    municipio,
    raca_cor,
    deficiencia,
    conclusao_curso_supeiror,
    ano_conclusao,
    rede_publica,
  }) => {
    const formaPasso1 = JSON.parse(localStorage.getItem("form-passo1"));
    return {
      nome: nome || (formaPasso1 ? formaPasso1.values.nome : ""),
      cpf: cpf || (formaPasso1 ? formaPasso1.values.cpf : ""),
      rg: rg || (formaPasso1 ? formaPasso1.values.rg : ""),
      dt_nascimento:
        dt_nascimento ||
        (formaPasso1 ? formaPasso1.values.dt_nascimento : null),
      genero: genero || (formaPasso1 ? formaPasso1.values.genero : ""),
      orgao_emissor:
        orgao_emissor || (formaPasso1 ? formaPasso1.values.orgao_emissor : ""),
      dt_emissao:
        dt_emissao || (formaPasso1 ? formaPasso1.values.dt_emissao : null),
      estado_civil:
        estado_civil || (formaPasso1 ? formaPasso1.values.estado_civil : ""),
      email: email || (formaPasso1 ? formaPasso1.values.email : ""),
      tel_residencial:
        tel_residencial ||
        (formaPasso1 ? formaPasso1.values.tel_residencial : ""),
      tel_celular:
        tel_celular || (formaPasso1 ? formaPasso1.values.tel_celular : ""),
      cep: cep || (formaPasso1 ? formaPasso1.values.cep : ""),
      endereco: endereco || (formaPasso1 ? formaPasso1.values.endereco : ""),
      numero: numero || (formaPasso1 ? formaPasso1.values.numero : ""),
      complemento:
        complemento || (formaPasso1 ? formaPasso1.values.complemento : ""),
      bairro: bairro || (formaPasso1 ? formaPasso1.values.bairro : ""),
      uf: uf || (formaPasso1 ? formaPasso1.values.uf : ""),
      municipio: municipio || (formaPasso1 ? formaPasso1.values.municipio : ""),
      raca_cor: raca_cor || (formaPasso1 ? formaPasso1.values.raca_cor : ""),
      deficiencia:
        deficiencia || (formaPasso1 ? formaPasso1.values.deficiencia : ""),
      conclusao_curso_supeiror:
        conclusao_curso_supeiror ||
        (formaPasso1 ? formaPasso1.values.conclusao_curso_supeiror : ""),
      ano_conclusao:
        ano_conclusao || (formaPasso1 ? formaPasso1.values.ano_conclusao : ""),
      rede_publica:
        rede_publica || (formaPasso1 ? formaPasso1.values.rede_publica : 0),
    };
  },

  validationSchema: Yup.object().shape({
    nome: Yup.string().required("Campo obrigatório"),
    cpf: Yup.string().required("Campo obrigatório"),
    rg: Yup.number().required("Campo obrigatório"),
    dt_nascimento: Yup.date()
      .nullable()
      .required("Campo obrigatório")
      .default(() => new Date()),
    genero: Yup.string().required("Campo obrigatório"),
    orgao_emissor: Yup.string().required("Campo obrigatório"),
    dt_emissao: Yup.date().nullable().required("Campo obrigatório"),
    estado_civil: Yup.string().required("Campo obrigatório"),
    email: Yup.string()
      .email("Insira um email válido")
      .required("Campo obrigatório"),
    tel_residencial: Yup.string().required("Campo obrigatório"),
    tel_celular: Yup.string().required("Campo obrigatório"),
    cep: Yup.string().required("Campo obrigatório"),
    endereco: Yup.string().required("Campo obrigatório"),
    numero: Yup.string().required("Campo obrigatório"),
    bairro: Yup.string().required("Campo obrigatório"),
    uf: Yup.string().required("Campo obrigatório"),
    municipio: Yup.string().required("Campo obrigatório"),
    raca_cor: Yup.string().required("Campo obrigatório"),
    deficiencia: Yup.string().required("Campo obrigatório"),
    conclusao_curso_supeiror: Yup.string().required("Campo obrigatório"),
    ano_conclusao: Yup.string().max(4).required("Campo obrigatório"),
    rede_publica: Yup.number().required("Campo obrigatório"),
  }),
  handleSubmit: (values, { props, setSubmitting }) => {
    setSubmitting(false);
    props.openSucesso();
    setTimeout(() => {
      props.onClickNext(values);
    }, 1000);
  },
})(withStyles(styles)(form));

export { FormFormik };
export default FormFormik;
