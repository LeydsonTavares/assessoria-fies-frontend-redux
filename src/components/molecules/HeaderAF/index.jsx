import React, { PureComponent as Component } from "react";
import { Img, Header } from "../../atoms";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import logo from "../../atoms/Img/imgfiles/logo1.png";

class HeaderAF extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Header>
        <Container>
          <Navbar
            className="justify-content-between"
            collapseOnSelect
            expand="lg"
            bg="transparent"
            variant="dark"
          >
            <Navbar.Brand href="#home">
              <Img
                src={logo}
                maxwidth={"35px"}
                maxheight={"35px"}
                alt="React Bootstrap logo"
              />
              {" Assessoria FIES"}
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="#home">Home</Nav.Link>
                <Nav.Link href="#sobre">Sobre FIES</Nav.Link>
                <Nav.Link href="#documentacao">Documentação</Nav.Link>
                <Nav.Link href="#assessoria">Conheça nossa assessoria</Nav.Link>
                <Nav.Link href="#contato">Contato</Nav.Link>
                <Nav.Link href="#noticia">Notícias</Nav.Link>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </Container>
      </Header>
    );
  }
}
export { HeaderAF };
export default HeaderAF;
