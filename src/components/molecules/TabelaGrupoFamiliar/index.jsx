import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import moment from "moment";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#008181",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

const TabelaGrupoFamiliar = (props) => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell align="left">CPF</StyledTableCell>
            <StyledTableCell align="left">Nome</StyledTableCell>
            <StyledTableCell align="left">Data de nascimento</StyledTableCell>
            <StyledTableCell align="left">Grau de parentesco</StyledTableCell>
            <StyledTableCell align="left">Renda individual</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.grupoFamiliar &&
            props.grupoFamiliar.length > 0 &&
            props.grupoFamiliar.map((row, index) => (
              <StyledTableRow key={index}>
                <StyledTableCell component="th" scope="row">
                  {row.cpf}
                </StyledTableCell>
                <StyledTableCell align="left">{row.nome}</StyledTableCell>
                <StyledTableCell align="left">
                  {moment(row.dt_nascimento).format("DD-MM-YYYY")}
                </StyledTableCell>
                <StyledTableCell align="left">
                  {row.grau_parentesco}
                </StyledTableCell>
                <StyledTableCell align="left">{row.renda}</StyledTableCell>
              </StyledTableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export { TabelaGrupoFamiliar };
export default TabelaGrupoFamiliar;
