import React, { useState } from "react";
import { Paper, Button, Grid, makeStyles } from "@material-ui/core";
import TabelaGrupoFamiliar from "../TabelaGrupoFamiliar";
import FormFormik from "./formFormik";

const useStyles = makeStyles(() => ({
  card: {
    padding: "2%",
  },
  buttonNext: {
    backgroundColor: "#fd970f",
    "&:hover": {
      backgroundColor: "#008181",
    },
    width: "100px",
  },
  buttonBack: {
    backgroundColor: "#008181",
    "&:hover": {
      backgroundColor: "#fd970f",
    },
    marginRight: "24px",
    width: "100px",
  },
}));

const FormGrupoFamiliar = (props) => {
  const classes = useStyles();
  const [grupoFamiliar, setGrupoFamiliar] = useState([]);

  const next = () => {
    props.onClickNext({ ...props.objPasso1, grupoFamiliar });
  };

  const back = () => {
    props.onClickBack();
  };

  return (
    <Paper className={classes.card} elevation={0}>
      <FormFormik
        setGrupoFamiliar={(obj) =>
          setGrupoFamiliar((grupoFamiliar) => [...grupoFamiliar, obj])
        }
      ></FormFormik>

      <Grid style={{ marginTop: "24px", marginBottom: "24px" }} item xs={12}>
        <TabelaGrupoFamiliar
          id="grupoFamiliar"
          grupoFamiliar={grupoFamiliar}
        ></TabelaGrupoFamiliar>
      </Grid>
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="center"
        item
        xs={12}
      >
        <Button
          variant="contained"
          className={classes.buttonBack}
          color="primary"
          onClick={() => back()}
        >
          Voltar
        </Button>
        <Button
          className={classes.buttonNext}
          variant="contained"
          color="primary"
          onClick={() => next()}
        >
          Avançar
        </Button>
      </Grid>
    </Paper>
  );
};

export { FormGrupoFamiliar };
export default FormGrupoFamiliar;
