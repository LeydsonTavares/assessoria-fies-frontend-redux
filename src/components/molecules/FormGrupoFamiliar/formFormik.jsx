import React from "react";
import { withFormik } from "formik";
import * as Yup from "yup";
import {
  CardActions,
  TextField,
  withStyles,
  Fab,
  Grid,
  MenuItem,
} from "@material-ui/core";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { cpfMask } from "../../../helpers/_mask";
import { grauParentesco } from "../../../helpers/_dataJson";
import AddIcon from "@material-ui/icons/Add";

const styles = () => ({
  alignLeft: {
    textAlign: "left",
  },
  actions: {
    float: "right",
  },
});

const form = (props) => {
  const {
    classes,
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = props;

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <form onSubmit={handleSubmit}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <KeyboardDatePicker
              autoOk
              style={{ width: "100%" }}
              variant="inline"
              id="dt_nascimento"
              inputVariant="outlined"
              label="Data de nascimento *"
              format="dd/MM/yyyy"
              margin="dense"
              value={values.dt_nascimento}
              onBlur={handleBlur}
              helperText={touched.dt_nascimento ? errors.dt_nascimento : ""}
              error={touched.dt_nascimento && Boolean(errors.dt_nascimento)}
              selected={values.dt_nascimento}
              onChange={(date) => setFieldValue("dt_nascimento", date)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              onChange={handleChange("cpf")}
              onBlur={handleBlur}
              helperText={touched.cpf ? errors.cpf : ""}
              error={touched.cpf && Boolean(errors.cpf)}
              id="cpf"
              margin="dense"
              label="CPF"
              inputProps={{ maxLength: 14 }}
              type="text"
              value={cpfMask(values.cpf)}
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              onChange={handleChange("nome")}
              margin="dense"
              onBlur={handleBlur}
              helperText={touched.nome ? errors.nome : ""}
              error={touched.nome && Boolean(errors.nome)}
              id="nome"
              label="Nome completo"
              type="text"
              value={values.nome}
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              select
              className={classes.alignLeft}
              id="grau_parentesco"
              label="Grau de parentesco"
              value={values.grau_parentesco}
              onChange={handleChange("grau_parentesco")}
              helperText={touched.grau_parentesco ? errors.grau_parentesco : ""}
              error={touched.grau_parentesco && Boolean(errors.grau_parentesco)}
              margin="dense"
              variant="outlined"
              fullWidth
            >
              {grauParentesco.map((option) => (
                <MenuItem key={option.id} value={option.value}>
                  {option.value}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              onChange={handleChange("renda")}
              margin="dense"
              onBlur={handleBlur}
              helperText={touched.renda ? errors.renda : ""}
              error={touched.renda && Boolean(errors.renda)}
              id="renda"
              label="Renda individual mensal bruta (R$) *"
              type="number"
              value={values.renda}
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12}>
            <CardActions className={classes.actions}>
              <Fab type="submit" color="primary" aria-label="add">
                <AddIcon />
              </Fab>
            </CardActions>
          </Grid>
        </Grid>
      </form>
    </MuiPickersUtilsProvider>
  );
};

const FormFormik = withFormik({
  mapPropsToValues: ({ dt_nascimento, cpf, nome, grau_parentesco, renda }) => {
    return {
      dt_nascimento: dt_nascimento || new Date(),
      cpf: cpf || "09654252406",
      nome: nome || "Leydson",
      grau_parentesco: grau_parentesco || "Pai",
      renda: renda || "10000",
    };
  },

  validationSchema: Yup.object().shape({
    dt_nascimento: Yup.date()
      .nullable()
      .required("Campo obrigatório")
      .default(() => new Date()),
    cpf: Yup.string().required("Campo obrigatório"),
    nome: Yup.string().required("Campo obrigatório"),
    grau_parentesco: Yup.string().required("Campo obrigatório"),
    renda: Yup.string().required("Campo obrigatório"),
  }),

  handleSubmit: (values, { props, setSubmitting }) => {
    props.setGrupoFamiliar(values);
  },
})(withStyles(styles)(form));

export { FormFormik };
export default FormFormik;
