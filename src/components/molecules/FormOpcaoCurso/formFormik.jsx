import React from "react";
import { withFormik } from "formik";
import * as Yup from "yup";
import {
  CardActions,
  TextField,
  withStyles,
  Fab,
  Grid,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { getEstado } from "../../../helpers/_dataJson";

const styles = () => ({
  alignLeft: {
    textAlign: "left",
  },
  actions: {
    float: "right",
  },
});

const form = (props) => {
  const {
    classes,
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <Autocomplete
            id="estado"
            options={getEstado}
            getOptionLabel={(option) => option.nome}
            onChange={(e, value) => {
              setFieldValue("estado", value.id);
            }}
            onBlur={handleBlur}
            renderInput={(params) => (
              <TextField
                {...params}
                helperText={touched.estado ? errors.estado : ""}
                error={touched.estado && Boolean(errors.estado)}
                fullWidth
                margin="dense"
                label="Estado"
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            onChange={handleChange("municipio")}
            onBlur={handleBlur}
            helperText={touched.municipio ? errors.municipio : ""}
            error={touched.municipio && Boolean(errors.municipio)}
            id="municipio"
            margin="dense"
            label="Munícipio"
            type="text"
            value={values.municipio}
            fullWidth
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            onChange={handleChange("curso")}
            margin="dense"
            onBlur={handleBlur}
            helperText={touched.curso ? errors.curso : ""}
            error={touched.curso && Boolean(errors.curso)}
            id="curso"
            label="Curso"
            type="text"
            value={values.curso}
            fullWidth
            variant="outlined"
          />
        </Grid>
        {/* <Grid item xs={12} sm={6}>
          <TextField
            select
            className={classes.alignLeft}
            id="grau_parentesco"
            label="Grau de parentesco"
            value={values.grau_parentesco}
            onChange={handleChange("grau_parentesco")}
            helperText={touched.grau_parentesco ? errors.grau_parentesco : ""}
            error={touched.grau_parentesco && Boolean(errors.grau_parentesco)}
            margin="dense"
            variant="outlined"
            fullWidth
          >
            {grauParentesco.map((option) => (
              <MenuItem key={option.id} value={option.value}>
                {option.value}
              </MenuItem>
            ))}
          </TextField>
        </Grid> */}
        <Grid item xs={12} sm={6}>
          <TextField
            onChange={handleChange("ies")}
            margin="dense"
            onBlur={handleBlur}
            helperText={touched.ies ? errors.ies : ""}
            error={touched.ies && Boolean(errors.ies)}
            id="ies"
            label="Instituição de Ensino Superior *"
            type="text"
            value={values.ies}
            fullWidth
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            onChange={handleChange("local")}
            margin="dense"
            onBlur={handleBlur}
            helperText={touched.local ? errors.local : ""}
            error={touched.local && Boolean(errors.local)}
            id="local"
            label="Local *"
            type="text"
            value={values.local}
            fullWidth
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <CardActions className={classes.actions}>
            <Fab type="submit" color="primary" aria-label="add">
              <AddIcon />
            </Fab>
          </CardActions>
        </Grid>
      </Grid>
    </form>
  );
};

const FormFormik = withFormik({
  mapPropsToValues: ({ estado, municipio, curso, ies, local }) => {
    return {
      estado: estado || "",
      municipio: municipio || "",
      curso: curso || "",
      ies: ies || "",
      local: local || "",
    };
  },

  validationSchema: Yup.object().shape({
    estado: Yup.string().required("Campo obrigatório"),
    municipio: Yup.string().required("Campo obrigatório"),
    curso: Yup.string().required("Campo obrigatório"),
    ies: Yup.string().required("Campo obrigatório"),
    local: Yup.string().required("Campo obrigatório"),
  }),

  handleSubmit: (values, { props, setSubmitting }) => {
    props.setGrupoPreferencia(values);
  },
})(withStyles(styles)(form));

export { FormFormik };
export default FormFormik;
