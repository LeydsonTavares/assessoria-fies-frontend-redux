import React, { useState } from "react";
import { Paper, Button, Grid, makeStyles } from "@material-ui/core";

import CardOpCurso from "../CardOpcaoCurso";
import FormFormik from "./formFormik";

const useStyles = makeStyles(() => ({
  card: {
    padding: "2%",
  },
  buttonNext: {
    backgroundColor: "#fd970f",
    "&:hover": {
      backgroundColor: "#008181",
    },
    width: "100px",
  },
  buttonBack: {
    backgroundColor: "#008181",
    "&:hover": {
      backgroundColor: "#fd970f",
    },
    marginRight: "24px",
    width: "100px",
  },
}));

const FormOpcaoCurso = (props) => {
  const classes = useStyles();
  const [grupoPreferencia, setGrupoPreferencia] = useState([]);

  const next = () => {
    props.onClickNext({ ...props.objPasso2, grupoPreferencia });
  };
  const back = () => {
    props.onClickBack();
  };

  return (
    <Paper className={classes.card} elevation={0}>
      <FormFormik
        setGrupoPreferencia={(obj) =>
          setGrupoPreferencia((grupoPreferencia) => [...grupoPreferencia, obj])
        }
      />
      <Grid
        style={{ marginTop: "24px", marginBottom: "24px" }}
        item
        xs={12}
        sm={6}
      >
        {grupoPreferencia &&
          grupoPreferencia.length > 0 &&
          grupoPreferencia.map((item, index) => (
            <CardOpCurso key={index} opcao={item} id={index}></CardOpCurso>
          ))}
      </Grid>
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="center"
        item
        xs={12}
      >
        <Button
          variant="contained"
          className={classes.buttonBack}
          color="primary"
          onClick={() => back()}
        >
          Voltar
        </Button>
        <Button
          className={classes.buttonNext}
          variant="contained"
          color="primary"
          onClick={() => next()}
        >
          Gravar
        </Button>
      </Grid>
    </Paper>
  );
};

export { FormOpcaoCurso };
export default FormOpcaoCurso;
