import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import DashboardIcon from "@material-ui/icons/Dashboard";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import BarChartIcon from "@material-ui/icons/BarChart";
import LayersIcon from "@material-ui/icons/Layers";

import { useDispatch } from "react-redux";
import { actions } from "../../../redux/actions/clientesActions";

const ListItems = () => {
  const dispatch = useDispatch();

  return (
    <div>
      <ListItem
        button
        onClick={() => dispatch(actions.changeCurrentComponent("DASHBOARD"))}
      >
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItem>
      <ListItem
        button
        onClick={() => dispatch(actions.changeCurrentComponent("ADD_USER"))}
      >
        <ListItemIcon>
          <PersonAddIcon />
        </ListItemIcon>
        <ListItemText primary="Novo Cliente" />
      </ListItem>
      <ListItem
        button
        onClick={() => dispatch(actions.changeCurrentComponent("ADD_CLIENT"))}
      >
        <ListItemIcon>
          <GroupAddIcon />
        </ListItemIcon>
        <ListItemText primary="Novo Usuário" />
      </ListItem>
      <ListItem
        button
        onClick={() => dispatch(actions.changeCurrentComponent("REPORTS"))}
      >
        <ListItemIcon>
          <BarChartIcon />
        </ListItemIcon>
        <ListItemText primary="Reports Clientes" />
      </ListItem>
      <ListItem button>
        <ListItemIcon>
          <LayersIcon />
        </ListItemIcon>
        <ListItemText primary="Integrations" />
      </ListItem>
    </div>
  );
};

export { ListItems };
export default ListItems;
