import React, { Fragment } from "react";
import { Paper, Typography, makeStyles } from "@material-ui/core";
import Anychart from "anychart-react/dist/anychart-react";
import { Title } from "../../atoms";
import { realMask } from "../../../helpers/_mask";

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
  paper: {
    height: "250px",
    padding: "5%",
  },
});
const Saldo = (props) => {
  const classes = useStyles();
  return (
    <Paper className={classes.paper} elevation={1}>
      <Fragment>
        <Title fontcolor={"rgb(124, 134, 142)"} fontSize={16} align={"center"}>
          {"Saldo Final"}
        </Title>
        <Typography component="p" variant="h4">
          {realMask(props.value)}
        </Typography>
        <Anychart
          type={"bar"}
          id={"id_forma_pagamento"}
          palette={["#008181"]}
          title={"Forma de pagamento"}
          data={[
            ["Trasferência", 20],
            ["Boleto", 10],
            ["Cartão de crédito", 5],
          ]}
          height={160}
        />
      </Fragment>
    </Paper>
  );
};

export { Saldo };
export default Saldo;
