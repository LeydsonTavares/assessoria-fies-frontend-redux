import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import clsx from "clsx";
import {
  Paper,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  IconButton,
  Grid
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { Visibility, VisibilityOff } from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  input: {
    display: "none"
  },
  textField: {
    width: "100%"
  },
  padding: {
    padding: "2%"
  }
}));

const Deposits = () => {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    nome: "",
    password: "",
    email: "",
    showPassword: false,
    images: [],
    videos: []
  });

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const handleCapture = ({ target }) => {
    const fileReader = new FileReader();
    const name = "images";

    fileReader.readAsDataURL(target.files[0]);
    fileReader.onload = e => {
      setValues(prevState => ({
        [name]: [...prevState[name], e.target.result]
      }));
    };
  };
  return (
    <Paper className={classes.padding} variant="outlined">
      <form noValidate autoComplete="off">
        <Grid container spacing={3} justify="center">
          <Grid item xs={6} sm={2}>
            <Paper variant="outlined">
              <input
                accept="image/*"
                id="icon-button-photo"
                className={classes.input}
                onChange={handleCapture}
                type="file"
              />
              <label htmlFor="icon-button-photo">
                <IconButton color="default" component="span">
                  <PhotoCamera fontSize={"large"} />
                </IconButton>
              </label>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={10}>
            <TextField
              onChange={handleChange("nome")}
              id="outlined-basic"
              label="Nome"
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              onChange={handleChange("email")}
              id="outlined-basic"
              label="Email"
              fullWidth
              variant="outlined"
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl className={clsx(classes.textField)} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">
                Password
              </InputLabel>
              <OutlinedInput
                id="outlined-adornment-password"
                type={values.showPassword ? "text" : "password"}
                value={values.password}
                fullWidth
                onChange={handleChange("password")}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {values.showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
                labelWidth={70}
              />
            </FormControl>
          </Grid>
        </Grid>

        {/* <FormControl></FormControl>
        <FormControl>
          <TextField
            onChange={handleChange("email")}
            id="outlined-basic"
            label="Email"
            variant="outlined"
          />
        </FormControl>
        <FormControl
          className={clsx(
            classes.margin,
            classes.withoutLabel,
            classes.textField
          )}
        >
          <InputLabel htmlFor="outlined-adornment-password">
            Password
          </InputLabel>
          <OutlinedInput
            id="outlined-adornment-password"
            type={values.showPassword ? "text" : "password"}
            value={values.password}
            onChange={handleChange("password")}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                  edge="end"
                >
                  {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
            labelWidth={70}
          />
        </FormControl> */}
      </form>
    </Paper>
  );
};

export { Deposits };
export default Deposits;
