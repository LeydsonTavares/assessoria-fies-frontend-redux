import React from "react";
import PropTypes from "prop-types";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import clsx from "clsx";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Check from "@material-ui/icons/Check";
import StepConnector from "@material-ui/core/StepConnector";
import { Typography } from "@material-ui/core";
import {
  FormDadosCadastrais,
  FormGrupoFamiliar,
  FormOpcaoCurso,
} from "../../molecules";

const useQontoStepIconStyles = makeStyles({
  root: {
    color: "#eaeaf0",
    display: "flex",
    height: 22,
    alignItems: "center",
  },
  active: {
    color: "#784af4",
  },
  circle: {
    width: 8,
    height: 8,
    borderRadius: "50%",
    backgroundColor: "currentColor",
  },
  completed: {
    color: "#784af4",
    zIndex: 1,
    fontSize: 18,
  },
});

function QontoStepIcon(props) {
  const classes = useQontoStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? (
        <Check className={classes.completed} />
      ) : (
        <div className={classes.circle} />
      )}
    </div>
  );
}

QontoStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
};

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg, #008181 0%, #008181 50%, #295872 100%)",
    },
  },
  completed: {
    "& $line": {
      backgroundImage:
        "linear-gradient( 95deg, #008181 0%, #008181 50%, #295872 100%)",
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: "#eaeaf0",
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: "#ccc",
    zIndex: 1,
    color: "#fff",
    width: 30,
    height: 30,
    display: "flex",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  active: {
    backgroundImage:
      "linear-gradient( 95deg, #008181 0%, #008181 50%, #295872 100%)",
    boxShadow: "0 4px 10px 0 rgba(0,0,0,.25)",
  },
  completed: {
    backgroundImage:
      "linear-gradient( 95deg, #008181 0%, #008181 50%, #295872 100%)",
  },
});

function ColorlibStepIcon(props) {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <Check />,
    2: <Check />,
    3: <Check />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  button: {
    marginRight: theme.spacing(1),
  },
  buttonNext: {
    backgroundColor: "#fd970f",
    "&:hover": {
      backgroundColor: "#008181",
    },
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ["Dados Cadastrais", "Grupo Familiar", "Opções de Cursos"];
}

const CustomizedSteppers = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [objRequest, setObjRequest] = React.useState({});
  const steps = getSteps();

  console.log(JSON.stringify(objRequest));

  const handleNext = (obj) => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setObjRequest(obj);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const getStepContent = (step) => {
    switch (step) {
      case 0:
        return (
          <FormDadosCadastrais
            onClickNext={(obj) => handleNext(obj)}
            onClickBack={() => handleBack}
            activeStep={step}
            stepsLength={steps.length}
            objInitial={objRequest}
          />
        );
      case 1:
        return (
          <FormGrupoFamiliar
            onClickNext={(obj) => handleNext(obj)}
            onClickBack={() => handleBack()}
            objPasso1={objRequest}
            activeStep={step}
            stepsLength={steps.length}
          />
        );
      case 2:
        return (
          <FormOpcaoCurso
            onClickNext={(obj) => handleNext(obj)}
            onClickBack={() => handleBack()}
            activeStep={step}
            stepsLength={steps.length}
            objPasso2={objRequest}
          ></FormOpcaoCurso>
        );
      default:
        return null;
    }
  };

  return (
    <div className={classes.root}>
      <Stepper
        alternativeLabel
        activeStep={activeStep}
        connector={<ColorlibConnector />}
      >
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <Typography color="primary" variant="caption" align="left">
        Ao preencher ou alterar os dados abaixo, clique no botão "Avançar".
      </Typography>

      {/* Render Forms */}
      <div>{getStepContent(activeStep)}</div>

      <Typography color="primary" variant="caption" align="left">
        * Campos de preenchimento obrigátorio.
      </Typography>
    </div>
  );
};

export { CustomizedSteppers };
export default CustomizedSteppers;
