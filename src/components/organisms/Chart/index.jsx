import React from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer
} from "recharts";
import { Typography } from "@material-ui/core";

const data = [
  {
    periodo: "2019.1",
    quantidade: 55
  },
  {
    periodo: "2019.2",
    quantidade: 50
  },
  {
    periodo: "2020.1",
    quantidade: 60
  },
  {
    periodo: "2020.2",
    quantidade: 70
  }
];

const Chart = () => {
  return (
    <div style={{ width: "100%", height: 300 }}>
      <Typography>{"Clientes cadastrados por período"}</Typography>
      <ResponsiveContainer>
        <AreaChart
          data={data}
          margin={{
            top: 10,
            right: 30,
            left: 0,
            bottom: 10
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="periodo" />
          <YAxis />
          <Tooltip />
          <Area
            type="monotone"
            dataKey="quantidade"
            stroke="#008181"
            fill="#008181"
          />
        </AreaChart>
      </ResponsiveContainer>
    </div>
  );
};

export { Chart };
export default Chart;
