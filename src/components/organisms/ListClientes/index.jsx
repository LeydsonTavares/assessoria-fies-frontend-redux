import React, { useState, useEffect } from "react";
import MaterialTable from "material-table";
import { makeStyles } from "@material-ui/core/styles";
import { forwardRef } from "react";
import { AddBox, ArrowUpward } from "@material-ui/icons";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import Chip from "@material-ui/core/Chip";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { Paper, Grid } from "@material-ui/core";
import Saldo from "../Saldo";
import AnyChart from "anychart-react/dist/anychart-react";
import MonetizationOnIcon from "@material-ui/icons/MonetizationOn";
import InsertChartIcon from "@material-ui/icons/InsertChart";
import { useDispatch, useSelector } from "react-redux";
import { actions } from "../../../redux/actions/clientesActions";
import { selectors } from "../../../redux/selectors/clientesSelectors";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const useStyles = makeStyles(() => ({
  chipPendente: {
    backgroundColor: "#ff1100",
    width: "100px",
    color: "#ffffff",
    border: "1px solid #ff1100",
  },
  chipPago: {
    backgroundColor: "#44b700",
    width: "100px",
    color: "#ffffff",
    border: "1px solid #44b700",
  },
  gridBottom: {
    marginBottom: "20px",
  },
  paper: {
    height: "250px",
    padding: "3%",
  },
}));

const ListClientes = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const objRetornoBFF = useSelector(selectors.getClientes);
  const [state] = useState({
    columsMobile: [
      {
        title: "Nome",
        field: "dadosPessoais.nome",
      },
      {
        title: "Pagamento",
        field: "statusPagamento.pago",
        render: (rowData) => (
          <Chip
            className={
              rowData.statusPagamento.pago
                ? classes.chipPago
                : classes.chipPendente
            }
            size="small"
            variant="outlined"
            label={rowData.statusPagamento.pago ? "PAGO" : "PENDENTE"}
          />
        ),
      },
    ],
    columnsDesktop: [
      { title: "Nome", field: "dadosPessoais.nome" },
      { title: "E-mail", field: "dadosPessoais.email" },
      { title: "CPF", field: "dadosPessoais.cpf" },
      {
        title: "Pagamento",
        field: "statusPagamento.pago",
        render: (rowData) => (
          <Chip
            className={
              rowData.statusPagamento.pago
                ? classes.chipPago
                : classes.chipPendente
            }
            size="small"
            variant="outlined"
            label={rowData.statusPagamento.pago ? "PAGO" : "PENDENTE"}
          />
        ),
      },
    ],
  });

  useEffect(() => {
    dispatch(actions.getClientes());
  }, []);

  return (
    <div>
      <Grid className={classes.gridBottom} container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Paper className={classes.paper} elevation={1}>
            <AnyChart
              type={"pie"}
              id={"id_pagamento"}
              legend={{ itemsLayout: "vertical" }}
              palette={["#44b700", "#ff1100"]}
              title={"Status Pagamento"}
              data={[
                {
                  name: "Pagamento efetuado",
                  value: objRetornoBFF.pag_efetuados,
                  state: "selected",
                },
                {
                  name: "Pagamento pendente",
                  value: objRetornoBFF.pag_pendentes,
                },
              ]}
            />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Paper className={classes.paper} elevation={1}>
            <AnyChart
              id={"id_processo"}
              type={"pie"}
              legend={{ itemsLayout: "vertical" }}
              palette={["#008181", "#295872"]}
              title={"Status Processamento"}
              data={[
                { name: "Processo finalizado", value: 2 },
                { name: "Processo pendente", value: 1, state: "selected" },
              ]}
            />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={4}>
          <Saldo value={objRetornoBFF.valorTotal}></Saldo>
        </Grid>
      </Grid>
      <MaterialTable
        title="Clientes cadastrados"
        icons={tableIcons}
        columns={props.isMobile ? state.columsMobile : state.columnsDesktop}
        data={objRetornoBFF.clientes}
        localization={{
          pagination: {
            labelDisplayedRows: "{from}-{to} de {count}",
            labelRowsSelect: "linhas",
            firstTooltip: "Primeira página",
            previousTooltip: "Página anterior",
            nextTooltip: "Próxima página",
            lastTooltip: "Última página",
          },
          header: {
            actions: "Ações",
          },
          toolbar: {
            nRowsSelected: "{0} linha(s) selecionada",
            searchPlaceholder: "Pesquisar",
            searchTooltip: "Pesquisar",
          },
          body: {
            emptyDataSourceMessage: "Sem clientes cadastrados",
            filterRow: {
              filterTooltip: "Filter",
            },
          },
        }}
        options={{
          cellStyle: {
            width: props.isMobile ? "60%" : "25%",
          },
          headerStyle: {
            width: "100%",
          },
          showTextRowsSelected: false,
        }}
        actions={[
          {
            icon: () => <MonetizationOnIcon />,
            tooltip: "Pagamento",
            onClick: (event, rowData) => alert("You saved " + rowData.name),
          },
          {
            icon: () => <InsertChartIcon />,
            tooltip: "Classificação",
            onClick: (event, rowData) =>
              alert("You want to delete " + rowData.name),
          },
        ]}
      />
    </div>
  );
};
export { ListClientes };
export default ListClientes;
